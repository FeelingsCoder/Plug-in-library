﻿using System.Web.Mvc;

namespace EasyUI.Controllers
{
    public class HomeController : Controller
    {
        
        /// <summary>
        /// 主页
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        
    }
}