﻿using EasyUI.Services;
using System.Web.Mvc;

namespace EasyUI.Controllers
{
    public class TreeController : Controller
    {
        TreeService service = new TreeService();

        /// <summary>
        /// 加载树
        /// </summary>
        /// <returns></returns>
        public JsonResult LoadTree()
        {
            var result = Json(service.LoadTree());
            return result;
        }

        /// <summary>
        /// 异步加载树
        /// </summary>
        /// <returns></returns>
        public JsonResult LoadAsyncTree(string id)
        {
            var result = Json(service.LoadAsyncTree(id), JsonRequestBehavior.AllowGet);
            return result;
        }
    }
}