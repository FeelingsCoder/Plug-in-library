﻿function CreateTree(){
    $("#tree").tree({
        url: "/Tree/LoadTree",
        animate: false, //定义当节点展开折叠时是否显示动画效果。
        checkbox: true, //定义是否在每个节点前边显示复选框。
        onlyLeafCheck: false, //定义是否只在叶节点前显示复选框。	
        lines: false, //定义是否显示树线条。	
        dnd: false, //定义是否启用拖放。
        formatter: function (node) { //定义如何呈现节点文本。
            return node.text;
        },
        onClick: function (node) {
            $("#text1").html(node.id + " : " + node.text);
        }
    });
    $('#tree2').tree({
        url: '/Tree/LoadAsyncTree?id=',
        animate: true, //定义当节点展开折叠时是否显示动画效果。
        checkbox: false, //定义是否在每个节点前边显示复选框。
        onlyLeafCheck: false, //定义是否只在叶节点前显示复选框。	
        lines: true, //定义是否显示树线条。	
        dnd: false, //定义是否启用拖放。
        formatter: function (node) { //定义如何呈现节点文本。
            return node.text;
        },
        onClick: function (node) {
            $("#text2").html(node.id + " : " + node.text);
        }
    });
}
