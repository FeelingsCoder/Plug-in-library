﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyUI.Models
{
    public class Classification
    {
        public string Id { get; set; }
        public string ParendId { get; set; }
        public string Title { get; set; }
        public Classification Parent { get; set; }
        public List<Classification> Children { set; get; }
    }
}