﻿using EasyUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Utils;
using System.Threading;

namespace EasyUI.Services
{
    public class TreeService
    {
        
        #region 数据
        List<Classification> clist = new List<Classification>()
        {
            new Classification() { Id="10000", Title="中国", ParendId = "10000", },
            new Classification() { Id="11000", Title="华北地区", ParendId = "10000", },
            new Classification() { Id="11100", Title="北京", ParendId = "11000", },
            new Classification() { Id="11110", Title="东城区", ParendId = "11100", },
            new Classification() { Id="11111", Title="景山街道", ParendId = "11110", },
            new Classification() { Id="11112", Title="东华门街道", ParendId = "11110", },
            new Classification() { Id="11113", Title="交道口街道", ParendId = "11110", },
            new Classification() { Id="11120", Title="西城区", ParendId = "11100", },
            new Classification() { Id="11121", Title="西长安街街道", ParendId = "11120", },
            new Classification() { Id="11122", Title="新街口街道", ParendId = "11120", },
            new Classification() { Id="11123", Title="月坛街道", ParendId = "11120", },
            new Classification() { Id="11200", Title="天津", ParendId = "11000", },
            new Classification() { Id="11210", Title="和平区", ParendId = "11200", },
            new Classification() { Id="11211", Title="劝业场街道", ParendId = "11210", },
            new Classification() { Id="11212", Title="小白楼街道", ParendId = "11210", },
            new Classification() { Id="11213", Title="五大道街道", ParendId = "11210", },
            new Classification() { Id="11220", Title="河东区", ParendId = "11200", },
            new Classification() { Id="11221", Title="大王庄街道", ParendId = "11220", },
            new Classification() { Id="11222", Title="大直沽街道", ParendId = "11220", },
            new Classification() { Id="11223", Title="中山门街道", ParendId = "11220", },
            new Classification() { Id="12000", Title="华东地区", ParendId = "10000", },
            new Classification() { Id="12100", Title="上海", ParendId = "12000", },
            new Classification() { Id="12110", Title="黄浦区", ParendId = "12100", },
            new Classification() { Id="12111", Title="南京东路街道", ParendId = "12110", },
            new Classification() { Id="12112", Title="半淞园路街道", ParendId = "12110", },
            new Classification() { Id="12113", Title="小东门街道", ParendId = "12110", },
            new Classification() { Id="12120", Title="杨浦区", ParendId = "12100", },
            new Classification() { Id="12121", Title="四平路街道", ParendId = "12120", },
            new Classification() { Id="12122", Title="江浦路街道", ParendId = "12120", },
            new Classification() { Id="12123", Title="殷行街道", ParendId = "12120", },
            new Classification() { Id="12200", Title="山东", ParendId = "12000", },
            new Classification() { Id="12210", Title="潍坊市", ParendId = "12200", },
            new Classification() { Id="12211", Title="坊子区", ParendId = "12210", },
            new Classification() { Id="12212", Title="寿光市", ParendId = "12210", },
            new Classification() { Id="12220", Title="青岛市", ParendId = "12200", },
            new Classification() { Id="12221", Title="市南区", ParendId = "12220", },
            new Classification() { Id="12222", Title="市北区", ParendId = "12220", },
            new Classification() { Id="12223", Title="崂山区", ParendId = "12220", },
            new Classification() { Id="12224", Title="李沧区", ParendId = "12220", },

            new Classification() { Id="20000", Title="美国", ParendId = "20000", },
            new Classification() { Id="30000", Title="加拿大", ParendId = "30000", },
            new Classification() { Id="21100", Title="华盛顿", ParendId = "20000", },
            new Classification() { Id="21200", Title="纽约", ParendId = "20000", },
            new Classification() { Id="40000", Title="澳大利亚", ParendId = "40000", },

        };

        #endregion
        
        /// <summary>
        /// 加载树
        /// </summary>
        /// <returns></returns>
        public List<TreeNode> LoadTree()
        {
            Thread.Sleep(1000); //手动添加延迟

            List<Classification> list = clist.FindAll(p => p.Id == p.ParendId);
            //DirectoryInfo di = new DirectoryInfo(@"D:\项目\Web\");
            List<TreeNode> result = CreateTree(list);
            return result;
        }

        /// <summary>
        /// 根据列表创建树
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private List<TreeNode> CreateTree(List<Classification> list)
        {
            List<TreeNode> parents = new List<TreeNode>();
            
            foreach (var parent in list)
            {
                TreeNode parentNode = new TreeNode()
                {
                    text = parent.Title,
                    id = parent.Id,
                    attributes="",
                    state="open",
                    iconCls=""
                };
                List<Classification> children = clist.FindAll(p => p.ParendId == parent.Id && p.ParendId != p.Id);

                if (children.Count > 0)
                {
                    parentNode.children = CreateTree(children);
                    parentNode.state = "closed";
                }
                parents.Add(parentNode);
            }

            return parents;
        }
        
        /// <summary>
        /// 异步加载树 加载 id 的所有子节点
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<TreeNode> LoadAsyncTree(string id)
        {
            Thread.Sleep(1000); //手动添加延迟

            List<Classification> list = null;

            if (string.IsNullOrWhiteSpace(id))
            {
                list = clist.FindAll(p => p.Id == p.ParendId);
            }
            else
            {
                list = clist.FindAll(p => p.ParendId == id && p.Id != p.ParendId);
            }

            List<TreeNode> parents = new List<TreeNode>();
            foreach (var parent in list)
            {
                TreeNode parentNode = new TreeNode()
                {
                    text = parent.Title,
                    id = parent.Id,
                    children = new List<TreeNode>()
                };
                bool hasChildren = clist.Find(p => p.ParendId == parent.Id && p.ParendId != p.Id) != null;
                if(hasChildren)
                {
                    parentNode.state = "closed";
                }
                parents.Add(parentNode);
            }
            return parents;
        }
        
        /// <summary>
        /// 获取路径树
        /// </summary>
        /// <param name="list">根路径</param>
        /// <returns></returns>
        public List<TreeNode> GetPathTree(DirectoryInfo[] list ,int count = 1)
        {
            List<TreeNode> tree = new List<TreeNode>();
            foreach (var item in list)
            {
                TreeNode node = new TreeNode()
                {
                    id = count.ToString(),
                    text = item.Name,
                };
                var children = item.GetDirectories();

                
                if(children.Length > 0)
                {
                    node.children = GetPathTree(children, count+1);
                }
                else
                {
                    FileInfo[] files = item.GetFiles();
                    if(files.Length > 0)
                    {
                        List<TreeNode> childtree = new List<TreeNode>();

                        foreach (var file in files)
                        {
                            TreeNode childNode = new TreeNode()
                            {
                                id = count.ToString(),
                                text = file.Name,
                            };
                            childtree.Add(childNode);
                        }
                    }
                }
                tree.Add(node);
            }
            return tree;
        }

        #region 弃用的数据

        static List<Classification> templist = new List<Classification>()
        {
            new Classification()
            {
                Id = "10000",
                Title = "中国",
                Children=new List<Classification>()
                {
                    new Classification()
                    {
                        Id = "11000",
                        Title = "华北地区",
                        Children=new List<Classification>()
                        {
                            new Classification()
                            {
                                Id = "11100",
                                Title = "北京",
                                Children=new List<Classification>()
                                {
                                    new Classification()
                                    {
                                        Id="11110",
                                        Title="东城区",
                                        Children=new List<Classification>()
                                        {
                                            new Classification()
                                            {
                                                Id="11111",
                                                Title="景山街道",
                                            },
                                            new Classification()
                                            {
                                                Id="11112",
                                                Title="东华门街道",
                                            },
                                            new Classification()
                                            {
                                                Id="11113",
                                                Title="交道口街道",
                                            }
                                        }
                                    },
                                    new Classification()
                                    {
                                        Id="11120",
                                        Title="西城区",
                                        Children=new List<Classification>()
                                        {
                                            new Classification()
                                            {
                                                Id="11121",
                                                Title="西长安街街道",
                                            },
                                            new Classification()
                                            {
                                                Id="11122",
                                                Title="新街口街道",
                                            },
                                            new Classification()
                                            {
                                                Id="11123",
                                                Title="月坛街道",
                                            }
                                        }
                                    }
                                }
                            },
                            new Classification()
                            {
                                Id = "11200",
                                Title = "天津",
                                Children=new List<Classification>()
                                {
                                    new Classification()
                                    {
                                        Id="11210",
                                        Title="和平区",
                                        Children=new List<Classification>()
                                        {
                                            new Classification()
                                            {
                                                Id="11211",
                                                Title="劝业场街道",
                                            },
                                            new Classification()
                                            {
                                                Id="11212",
                                                Title="小白楼街道",
                                            },
                                            new Classification()
                                            {
                                                Id="11213",
                                                Title="五大道街道",
                                            }
                                        }
                                    },
                                    new Classification()
                                    {
                                        Id="11220",
                                        Title="河东区",
                                        Children=new List<Classification>()
                                        {
                                            new Classification()
                                            {
                                                Id="11221",
                                                Title="大王庄街道",
                                            },
                                            new Classification()
                                            {
                                                Id="11222",
                                                Title="大直沽街道",
                                            },
                                            new Classification()
                                            {
                                                Id="11223",
                                                Title="中山门街道",
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    new Classification()
                    {
                        Id = "12000",
                        Title = "华东地区",
                        Children=new List<Classification>()
                        {
                            new Classification()
                            {
                                Id = "12100",
                                Title = "上海",
                                Children=new List<Classification>()
                                {
                                    new Classification()
                                    {
                                        Id="12110",
                                        Title="黄浦区",
                                        Children=new List<Classification>()
                                        {
                                            new Classification()
                                            {
                                                Id="12111",
                                                Title="南京东路街道",
                                            },
                                            new Classification()
                                            {
                                                Id="12112",
                                                Title="半淞园路街道",
                                            },
                                            new Classification()
                                            {
                                                Id="12113",
                                                Title="小东门街道",
                                            }
                                        }
                                    },
                                    new Classification()
                                    {
                                        Id="12120",
                                        Title="杨浦区",
                                        Children=new List<Classification>()
                                        {
                                            new Classification()
                                            {
                                                Id="12121",
                                                Title="四平路街道",
                                            },
                                            new Classification()
                                            {
                                                Id="12122",
                                                Title="江浦路街道",
                                            },
                                            new Classification()
                                            {
                                                Id="12123",
                                                Title="殷行街道",
                                            }
                                        }
                                    }
                                }
                            },
                            new Classification()
                            {
                                Id = "12200",
                                Title = "山东",
                                Children=new List<Classification>()
                                {
                                    new Classification()
                                    {
                                        Id="12210",
                                        Title="潍坊市",
                                        Children=new List<Classification>()
                                        {
                                            new Classification()
                                            {
                                                Id="12211",
                                                Title="坊子区",
                                            },
                                            new Classification()
                                            {
                                                Id="12212",
                                                Title="寿光市",
                                            }
                                        }
                                    },
                                    new Classification()
                                    {
                                        Id="12220",
                                        Title="青岛市",
                                        Children=new List<Classification>()
                                        {
                                            new Classification()
                                            {
                                                Id="12221",
                                                Title="市南区",
                                            },
                                            new Classification()
                                            {
                                                Id="12222",
                                                Title="市北区",
                                            },
                                            new Classification()
                                            {
                                                Id="12223",
                                                Title="崂山区",
                                            },
                                            new Classification()
                                            {
                                                Id="12224",
                                                Title="李沧区",
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                }
            },
            new Classification()
            {
                Id="20000",
                Title="美国",
                Children=new List<Classification>()
                {
                    new Classification()
                    {
                        Id="21000",
                        Title="新英格兰地区",
                        Children= new List<Classification>()
                        {
                            new Classification()
                            {
                                Id="21100",
                                Title="缅因州"
                            },
                            new Classification()
                            {
                                Id="21200",
                                Title="新罕布什尔州"
                            },
                        }
                    },
                    new Classification()
                    {
                        Id="22000",
                        Title="中大西洋地区",
                        Children=new List<Classification>()
                        {
                            new Classification()
                            {
                                Id="22100",
                                Title="纽约州"
                            },
                            new Classification()
                            {
                                Id="22200",
                                Title="华盛顿特区"
                            },
                        }
                    }
                }
            }
        };


        #endregion
    }
}