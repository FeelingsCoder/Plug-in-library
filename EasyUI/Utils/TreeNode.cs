﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public class TreeNode
    {
        public string id { get; set; }
        public string text { get; set; }
        public string state { get; set; }
        public string @checked { get; set; }
        public string attributes { get; set; }
        public string iconCls { get; set; }
        public List<TreeNode> children { set; get; } = new List<TreeNode>();
    }
}
