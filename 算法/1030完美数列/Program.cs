﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1030完美数列
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> list = new List<string>()
            {
                "2级","13级","3级","22级"
            };

            var ll = from l in list
                     select new
                     {
                         name = l,
                         key = "123"
                     };
            Console.WriteLine(ll.Max(p=>p.name));
        }
    }
}
