﻿using Highcharts.Core;
using Highcharts.Core.Appearance;
using Highcharts.Core.Data.Chart;
using Highcharts.Core.Events;
using Highcharts.Core.Options;
using Highcharts.Core.PlotOptions;
using Highcharts.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace myui.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            Chart c = new Chart();
            

            ChartOptions cs = new ChartOptions();

            Serie s = new Serie();

            YAxisItem ss = new YAxisItem();
           
            ss.labels = new Labels();
            ss.labels.style = new CSSObject();
            var r = new PlotPointEvents();
            return View();

        }
        public override string ToString() { return base.ToString(); }
    }
}