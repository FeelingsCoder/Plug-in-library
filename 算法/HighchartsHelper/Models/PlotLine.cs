﻿using HighchartsHelper.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models
{
    public class PlotLine
    {
        public PlotLine() { }

        public string color { get; set; }
        public string dashStyle { get; set; }
        public PlotEvents events { get; set; }
        public string id { get; set; }
        public PlotLabel label { get; set; }
        public double? value { get; set; }
        public int? width { get; set; }
        public int? zIndex { get; set; }

        public override string ToString() { return base.ToString(); }
    }
}
