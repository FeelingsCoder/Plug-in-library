﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models
{
    public class Serie
    {
        public object[] center;
        public object[] data;
        public string id;
        public int? innerSize;
        public string name;
        public int? size;
        public string stack;
        public string type;
        public int? xAxis;
        public int? yAxis;

        public Serie() { }

        public string color { get; set; }
        public int? level { get; set; }
        public bool? selected { get; set; }
        public bool? showInLegend { get; set; }
        public bool? visible { get; set; }
        public override string ToString() { return base.ToString(); }
    }
}
