﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models
{
    public class Labels
    {
        /// <summary>
        /// 
        /// </summary>
        public Labels() { }
        /// <summary>
        /// 
        /// </summary>
        public string align { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool enabled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string formatter { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? rotation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? staggerLines { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? step { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public CSSObject style { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string verticalAlign { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? x { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? y { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public override string ToString() { return base.ToString(); }
    }
}
