﻿using HighchartsHelper.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models
{
    public class PlotBand
    {
        public PlotBand() { }

        public string color { get; set; }
        public PlotEvents events { get; set; }
        public double? from { get; set; }
        public string id { get; set; }
        public PlotLabel label { get; set; }
        public double? to { get; set; }
        public int? zIndex { get; set; }

        public override string ToString() { return base.ToString(); }
    }
}
