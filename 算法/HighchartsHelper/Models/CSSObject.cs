﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models
{
    public class CSSObject
    {
        /// <summary>
        /// 
        /// </summary>
        public CSSObject() { }

        /// <summary>
        /// 
        /// </summary>
        public string color { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string font { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string fontFamily { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string fontSize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string fontWeight { get; set; }
    }
}
