﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models
{
    public abstract class Axis
    {
        public int? tickWidth;

        /// <summary>
        /// 
        /// </summary>
        public Axis() { }
        /// <summary>
        /// 
        /// </summary>
        public bool? allowDecimals { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string alternateGridColor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string dateTimeLabelFormats { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? endOnTick { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string gridLineColor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string gridLineDashStyle { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? gridLineWidth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Labels labels { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string lineColor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? lineWidth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? linkedTo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? max { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double? maxPadding { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? maxZoom { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? min { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string minorGridLineColor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string minorGridLineDashStyle { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? minorGridLineWidth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string minorTickColor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? minorTickInterval { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? minorTickLength { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string minorTickPosition { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? minorTickWidth { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double? minPadding { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? offset { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? opposite { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public PlotBands plotBands { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public PlotLines plotLines { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? reversed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? showFirstLabel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? showLastLabel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DataLabels stackLabels { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? startOfWeek { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? startOnTick { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string tickColor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long? tickInterval { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? tickLength { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string tickmarkPlacement { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? tickPixelInterval { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string tickPosition { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Title title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string type { get; set; }
    }
}
