﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models
{
    public class DataLabels : Labels
    {
        public DataLabels() { }

        public string color { get; set; }

        public override string ToString() { return base.ToString(); }
    }
}
