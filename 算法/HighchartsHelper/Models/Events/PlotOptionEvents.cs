﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models.Events
{
    public class PlotOptionEvents
    {
        public PlotOptionEvents() { }

        public string click { get; set; }
        public string hide { get; set; }
        public string legendItemClick { get; set; }
        public string mouseOut { get; set; }
        public string mouseOver { get; set; }
        public string show { get; set; }

        public override string ToString() { return base.ToString(); }
    }
}
