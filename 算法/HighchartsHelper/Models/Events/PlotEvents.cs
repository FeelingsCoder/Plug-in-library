﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models.Events
{
    public class PlotEvents
    {
        public PlotEvents() { }

        public string click { get; set; }
        public string mouseMove { get; set; }
        public string mouseOut { get; set; }
        public string mouseOver { get; set; }

        public override string ToString() { return base.ToString(); }
    }
}
