﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models.Events
{
    public class Points
    {
        public Points() { }

        public string click { get; set; }
        public string mouseOut { get; set; }
        public string mouseOver { get; set; }
        public string remove { get; set; }
        public string select { get; set; }
        public string unselect { get; set; }
        public string update { get; set; }

        public override string ToString() { return base.ToString(); }
    }
}
