﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models
{
    public class PlotLabel : Labels
    {
        public PlotLabel() { }

        public string text { get; set; }
    }
}
