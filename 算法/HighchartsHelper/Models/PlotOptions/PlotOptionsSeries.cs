﻿using HighchartsHelper.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models.PlotOptions
{
    public abstract class PlotOptionsSeries
    {
        protected PlotOptionsSeries() { }

        public bool? allowPointSelect { get; set; }
        public bool? animation { get; set; }
        public string color { get; set; }
        public bool? connectNulls { get; set; }
        public string cursor { get; set; }
        public string dashStyle { get; set; }
        public DataLabels dataLabels { get; set; }
        public bool? enableMouseTracking { get; set; }
        public PlotOptionEvents events { get; set; }
        public string id { get; set; }
        public int? lineWidth { get; set; }
        //public Marker marker { get; set; }
        public PlotPointEvents point { get; set; }
        public int? pointInterval { get; set; }
        public int? pointStart { get; set; }
        public bool? shadow { get; set; }
        public bool? showInLegend { get; set; }
        public string stacking { get; set; }
        public SerieStates states { get; set; }
        public bool? stickyTracking { get; set; }
        public bool? visible { get; set; }
        public int? zIndex { get; set; }
    }
}
