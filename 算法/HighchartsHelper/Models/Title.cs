﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models
{
    public class Title
    {
        public Title(string titleText)
        {
            text = titleText;
        }

        public string align { get; set; }
        public int? margin { get; set; }
        public int? rotation { get; set; }
        public CSSObject style { get; set; }
        public string text { get; set; }

        public override string ToString() { return base.ToString(); }
    }
}
