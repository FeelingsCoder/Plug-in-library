﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models
{
    /// <summary>
    /// 导出功能
    /// </summary>
    public class Exporting
    {
        public Exporting() { }
        
        /// <summary>
        /// 是否启用导出功能
        /// </summary>
        public bool? enabled { get; set; }

        public override string ToString() { return base.ToString(); }
    }
}
