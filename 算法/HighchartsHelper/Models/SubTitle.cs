﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models
{
    public class SubTitle : Title
    {
        public SubTitle(string subTitleText) : base(subTitleText) { }

        public override string ToString() { return base.ToString(); }
    }
}
