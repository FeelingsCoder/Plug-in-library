﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models
{
    public class Series : List<Serie>
    {
        public Series() { }

        public DataLabels dataLabels { get; set; }

        public override string ToString() { return base.ToString(); }
    }
}
