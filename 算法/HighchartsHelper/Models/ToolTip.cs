﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper.Models
{
    public class ToolTip
    {
        public ToolTip() { }
        public ToolTip(string format) { }

        public string backgroundColor { get; set; }
        public string borderColor { get; set; }
        public int? borderRadius { get; set; }
        public int? borderWidth { get; set; }
        public bool? crosshairs { get; set; }
        public bool? enabled { get; set; }
        public string formatter { get; set; }
        public bool? shadow { get; set; }
        public bool? shared { get; set; }
        public int? snap { get; set; }
        public CSSObject style { get; set; }

        public override string ToString() { return base.ToString(); }
    }
}
