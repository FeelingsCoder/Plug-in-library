﻿using HighchartsHelper.Models;
using HighchartsHelper.Models.PlotOptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighchartsHelper
{
    public class ChartOptions
    {
        public ChartOptions() { }

        public AJAXSource AjaxDataSource { get; set; }
        public Appearance.Appearance Appearance { get; set; }
        public string ClientId { get; set; }
        public ColorSet Colors { get; set; }
        public Exporting Exporting { get; set; }
        public Localization Lang { get; set; }
        public Legend Legend { get; set; }
        public PlotOptionsSeries PlotOptions { get; set; }
        public string RenderType { get; set; }
        public Series Series { get; set; }
        public bool ShowCredits { get; set; }
        public SubTitle SubTitle { get; set; }
        public string Theme { get; set; }
        public Title Title { get; set; }
        public ToolTip Tooltip { get; set; }
        public XAxis XAxis { get; set; }
        public YAxis YAxis { get; set; }
    }
}
