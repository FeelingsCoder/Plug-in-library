﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.NPOI.Utils
{
    /// <summary>
    /// 返回的消息
    /// </summary>
    public class MessageResult
    {
        /// <summary>
        /// 返回消息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 返回的对象
        /// </summary>
        public object Result { get; set; }

        /// <summary>
        /// 返回状态
        /// </summary>
        public string State { get; set; }
    }
}

//var list = Request.Files;
//string state = "";
//if (file != null && file.ContentLength > 0 )
//{
//    if (file.ContentType == "application/vnd.ms-excel")
//    {
//        HSSFWorkbook hssfworkbook = new HSSFWorkbook(file.InputStream);
//        ISheet sheet = hssfworkbook.GetSheetAt(0);
//        if(sheet != null)
//            state = ToModels(sheet, users);
//    }
//    else if(file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
//    {
//        XSSFWorkbook xssfworkbook = new XSSFWorkbook(file.InputStream);
//        ISheet sheet = xssfworkbook.GetSheetAt(0);
//        if (sheet != null)
//            state = ToModels(sheet, users);
//    }
//    else
//    {
//        state = "文件格式不正确";
//    }
//}
//else
//{
//    state = "文件不能为空";
//}

/*
 * 
    /// <summary>
    /// 将 Excel 表 转化为 Model List 特别说明：Model 中的属性类型应为 string
    /// </summary>
    /// <typeparam name="T">Model</typeparam>
    /// <param name="sheet">表</param>
    /// <param name="list">list</param>
    /// <returns></returns>
    private string ToModels<T>(ISheet sheet, List<T> list) where T : class
    {
        string state = "";
        if (sheet != null && sheet.LastRowNum >= 0)
        {
            Type type = typeof(T);
            PropertyInfo[] properties = type.GetProperties();
            IRow firstRow = sheet.GetRow(0);

            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            for (int i = 0; i < firstRow.LastCellNum; i++)
            {
                ICell cell = firstRow.GetCell(i);
                if (cell != null) dictionary.Add(cell.ToString().ToUpper(), i);
            }
            foreach (var property in properties)
            {
                string key = dictionary.Keys.FirstOrDefault(p => p.Equals(property.Name, StringComparison.CurrentCultureIgnoreCase));
                if (key == null)
                {
                    state += $"缺少 {property.Name} 列的信息\n";
                }
            }
            if (string.IsNullOrWhiteSpace(state))
            {
                for (int i = 1; i <= sheet.LastRowNum; i++)
                {
                    IRow row = sheet.GetRow(i);
                    if (row != null)
                    {
                        T obj = Activator.CreateInstance<T>();
                        foreach (var property in properties)
                        {
                            ICell cell = row.GetCell(dictionary[property.Name.ToUpper()]);
                            property.SetValue(obj, cell == null ? null : cell.ToString(), null);
                        }
                        list.Add(obj);
                    }
                }
            }
        }
        else state = "表不能为空";
        return state;
    }
 * **/
