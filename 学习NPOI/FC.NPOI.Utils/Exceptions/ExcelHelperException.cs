﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.NPOI.Utils.Exceptions
{
    /// <summary>
    /// Excel 文件异常
    /// </summary>
    public class ExcelHelperException : Exception
    {
        public ExcelHelperException() : base() { }
        public ExcelHelperException(string message) : base(message) { }
    }
}
