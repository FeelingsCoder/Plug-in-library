﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPOI_test_1
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder sbr = new StringBuilder();
            DataSet set = new DataSet();
            using (FileStream fs = File.OpenRead(@"e:/temp.xls"))   //打开myxls.xls文件
            {
                HSSFWorkbook wk = new HSSFWorkbook(fs);   //把xls文件中的数据写入wk中
                
                for (int i = 0; i < wk.NumberOfSheets; i++)  //NumberOfSheets是myxls.xls中总共的表数
                {
                    DataTable table = new DataTable();
                    ISheet sheet = wk.GetSheetAt(i);   //读取当前表数据

                    if(sheet.LastRowNum > 0)
                    {
                        IRow row = sheet.GetRow(0);
                        for (int k = 0; k < row.LastCellNum; k++)
                        {
                            table.Columns.Add(row.Cells[k].ToString(), System.Type.GetType("System.String"));
                        }
                    }

                    for (int j = 1; j <= sheet.LastRowNum; j++)  //LastRowNum 是当前表的总行数
                    {
                        IRow row = sheet.GetRow(j);  //读取当前行数据
                        if (row != null)
                        {
                            table.Rows.Add(row.ToArray());
                            //for (int k = 0; k <= row.LastCellNum; k++)  //LastCellNum 是当前行的总列数
                            //{
                            //    ICell cell = row.GetCell(k);  //当前表格
                            //    if (cell != null)
                            //    {
                            //    }
                            //}
                        }
                    }
                    set.Tables.Add(table);
                }
            }
            Console.WriteLine(sbr.ToString());
        }
    }
}
