﻿using FC.Web.Data;
using FC.Web.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FC.Web.Controllers
{
    public class HomeController : Controller
    {
        FCDBContext context = new FCDBContext();
        /// <summary>
        /// 主页
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            FormInfo form = new FormInfo()
            {
                Id = "10086",
                Name = "请假单",
                Items = new List<FormItemInfo>()
                {
                    new FormItemInfo()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Name = "请假理由",
                    },
                    new FormItemInfo()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Name = "请假时间",
                    },
                }
            };
            try
            {
                context.Forms.Add(form);
                context.SaveChanges();
            }
            catch (Exception)
            {

            }
            return View();
        }

        /// <summary>
        /// 添加流程页面
        /// </summary>
        /// <returns></returns>
        public ActionResult AddProcedure()
        {
            ProcedureInfo procedure = new ProcedureInfo();
            procedure.Id = Guid.NewGuid().ToString();
            if (Session["Procedure"] == null)
                Session.Add("Procedure", procedure.Id);
            else Session["Procedure"] = procedure.Id;
            return View(procedure);
        }

        /// <summary>
        /// 添加流程表单提交
        /// </summary>
        /// <param name="procedure"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddProcedure(ProcedureInfo procedure)
        {
            bool ok = true;
            try
            {
                context.Procedures.Add(procedure);
                context.SaveChanges();
            }
            catch(Exception e)
            {
                ok = false;
            }
            if(ok)
            {
                if (Session["Number"] == null)
                {
                    Session.Add("Number", 1);
                }
                else Session["Number"] = 1;
                return RedirectToAction("AddStep");
            }
            return View(procedure);
        }

        /// <summary>
        /// 添加步骤页面
        /// </summary>
        /// <returns></returns>
        public ActionResult AddStep()
        {
            StepInfo step = new StepInfo();
            step.Id = Guid.NewGuid().ToString();
            step.Number = int.Parse(Session["Number"].ToString());
            step.ProcedureId = Session["Procedure"].ToString();
            return View(step);
        }

        /// <summary>
        /// 添加步骤表单提交
        /// </summary>
        /// <param name="step"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddStep(StepInfo step)
        {
            bool ok = true;
            try
            {
                context.Steps.Add(step);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                ok = false;
            }
            if (ok)
            {
                Session["Number"] = step.Number + 1;
                return RedirectToAction("AddStep");
            }
            return View(step);
        }
    }
}