﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FC.Web.Data.Models
{
    /// <summary>
    /// 流程中具体步骤
    /// </summary>
    public class StepInfo
    {
        /// <summary>
        /// 步骤Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 所属流程Id
        /// </summary>
        public string ProcedureId { get; set; }

        /// <summary>
        /// 所需填写的表单Id
        /// </summary>
        public string FormId { get; set; }

        /// <summary>
        /// 步骤名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 步骤编号
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// 导航属性：所属流程
        /// </summary>
        public ProcedureInfo Procedure { get; set; }

        /// <summary>
        /// 导航属性：所需填写的表单
        /// </summary>
        public FormInfo Form { get; set; }
    }
}