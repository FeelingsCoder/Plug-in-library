﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FC.Web.Data.Models
{
    /// <summary>
    /// 表单信息
    /// </summary>
    public class FormInfo
    {
        /// <summary>
        /// 表单Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 表单名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 导航属性：表单项
        /// </summary>
        public List<FormItemInfo> Items { get; set; }
    }
}