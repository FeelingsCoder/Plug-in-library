﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FC.Web.Data.Models
{
    /// <summary>
    /// 流程信息
    /// </summary>
    public class ProcedureInfo
    {
        /// <summary>
        /// 流程编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 流程名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 导航属性：步骤
        /// </summary>
        public List<StepInfo> Steps { get; set; }
    }
}