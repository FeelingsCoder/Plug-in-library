﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FC.Web.Data.Models
{
    /// <summary>
    /// 表单项信息
    /// </summary>
    public class FormItemInfo
    {
        /// <summary>
        /// 项目编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 所属表单Id
        /// </summary>
        public string FormId { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 填写的值
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 导航属性：所属表单
        /// </summary>
        public FormInfo Form { get; set; }
    }
}