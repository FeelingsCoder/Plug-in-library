﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FC.Web.Data.Models
{
    /// <summary>
    /// 流程状态
    /// </summary>
    public class Info
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Id { set; get; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 流程Id
        /// </summary>
        public string ProcedureId { get; set; }

        /// <summary>
        /// 当前步骤Id
        /// </summary>
        public string StepId { get; set; }

        /// <summary>
        /// 当前状态 待审批 通过 驳回
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// 导航属性：用户信息
        /// </summary>
        public UserInfo User { get; set; }

        /// <summary>
        /// 导航属性：流程信息
        /// </summary>
        public ProcedureInfo Procrdure { get; set; }

        /// <summary>
        /// 导航属性：步骤信息
        /// </summary>
        public StepInfo Step { get; set; }
    }
}