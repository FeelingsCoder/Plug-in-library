﻿using FC.Web.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace FC.Web.Data
{
    /// <summary>
    /// 数据库上下文
    /// </summary>
    public class FCDBContext : DbContext
    {
        /// <summary>
        /// 用户信息表
        /// </summary>
        public DbSet<UserInfo> Users { get; set; }

        /// <summary>
        /// 步骤信息
        /// </summary>
        public DbSet<StepInfo> Steps { get; set; }

        /// <summary>
        /// 流程信息
        /// </summary>
        public DbSet<ProcedureInfo> Procedures { get; set; }

        /// <summary>
        /// 表单项
        /// </summary>
        public DbSet<FormItemInfo> FormItems { get; set; }

        /// <summary>
        /// 表单信息表
        /// </summary>
        public DbSet<FormInfo> Forms { get; set; }

        /// <summary>
        /// 流程状态
        /// </summary>
        public DbSet<Info> Infos { get; set; }
        

        #region 加载实体映射

        /// <summary>
        /// 方法重写
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //加载实体映射
            //modelBuilder.Configurations.Add(new BsLineRepairRangeMap());

            modelBuilder.Properties().Where(p => p.Name == "RowVersion" || p.Name == "RowVersion".ToUpper()).Configure(p => p.IsConcurrencyToken());
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();//移除复数表名的契约

            //modelBuilder.Types().Configure(f => f.ToTable(("t_" + f.ClrType).ToUpper(Infran.GetDatabaseType()));  
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();  //表中都统一设置禁用一对多级联删除
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>(); //表中都统一设置禁用多对多级联删除
            base.OnModelCreating(modelBuilder);
        }
        #endregion
    }
}