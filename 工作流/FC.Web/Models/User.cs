﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FC.Web.Models
{
    /// <summary>
    /// 用户信息视图模型，保存在Session中
    /// </summary>
    public class User
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
    }
}