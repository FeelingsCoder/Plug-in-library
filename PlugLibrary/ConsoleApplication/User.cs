﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    public class User
    {
        public int Id { get; set; }
        public int A { get; set; }
        public int B { get; set; }
        public string C { get; set; }
        public int D { get; set; }
    }
    public class UserInfo
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int A { get; set; }
        public int B { get; set; }
        public string C { get; set; }
        public int D { get; set; }
    }
}
