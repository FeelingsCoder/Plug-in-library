﻿using FC.Math;
using FC.Strings;
using FC.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {

            #region 测试自动类型转换
            //User u = new User()
            //{
            //    A = 1,
            //    B = 2,
            //    C = "哈哈",
            //    D = 3,
            //    Id = 10086
            //};
            //UserInfo ui = new UserInfo();
            //ui = u.Convert(ui);
            #endregion

            #region 测试进制转换

            //Console.WriteLine(1024.ToBinary());
            //Console.WriteLine(1024.ToOctal());
            //Console.WriteLine(1023.ToHex(false));

            #endregion

            #region 测试字符串栈
            
            Console.Write("StackStringBuilder: ");

            Stopwatch ss = new Stopwatch();

            ss.Start();

            StackStringBuilder ssBuilder = new StackStringBuilder();

            for (int i = 0; i < 1000000; i++)
                ssBuilder.Push("abcdefg");
            
            ss.Stop();

            Console.WriteLine(ss.Elapsed);

            //////

            Console.Write("String: ");

            Stopwatch s = new Stopwatch();

            s.Start();

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < 1000000; i++)
                sb.Append("abcdefg");
            
            s.Stop();
            
            Console.WriteLine(s.Elapsed);

            #endregion
        }
    }
}
