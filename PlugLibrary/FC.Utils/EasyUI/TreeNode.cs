﻿using System.Collections.Generic;

namespace FC.Utils.EasyUI
{
    /// <summary>
    /// EasyUI 树 节点类
    /// </summary>
    public class TreeNode
    {
        /// <summary>
        /// 默认构造方法
        /// </summary>
        public TreeNode() { }

        /// <summary>
        /// 根据 id 和 text 创建节点
        /// </summary>
        /// <param name="id">节点 id </param>
        /// <param name="text">节点文本</param>
        public TreeNode(string id, string text)
        {
            this.id = id;
            this.text = text;
        }

        /// <summary>
        /// 节点的 id，它对于加载远程数据很重要。
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 要显示的节点文本
        /// </summary>
        public string text { get; set; }

        /// <summary>
        /// 节点状态，'open' 或 'closed'，默认是 'open'。当设置为 'closed' 时，该节点有子节点，并且将从远程站点加载它们。
        /// </summary>
        public string state { get; set; } = TreeNodeState.NODE_STATE_OPEN;

        /// <summary>
        /// 节点图标
        /// </summary>
        public string iconCls { get; set; }

        /// <summary>
        /// 指示节点是否被选中
        /// </summary>
        public string @checked { get; set; }

        /// <summary>
        /// 给一个节点添加的自定义属性
        /// </summary>
        public string attributes { get; set; }

        /// <summary>
        /// 定义了一些子节点的节点数组
        /// </summary>
        public IList<TreeNode> children { get; set; }
    }
    
    /// <summary>
    /// 节点状态
    /// </summary>
    public struct TreeNodeState
    {
        /// <summary>
        /// 节点状态 打开
        /// </summary>
        public static string NODE_STATE_OPEN = "open";

        /// <summary>
        /// 节点状态 关闭
        /// </summary>
        public static string NODE_STATE_CLOSED = "closed";
    }
}
