﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Strings
{

    /// <summary>
    /// 字符串工具类
    /// </summary>
    public static class StringUtils
    {
        #region 反转一个字符串

        /// <summary>
        /// 反转一个字符串
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="startIndex">开始下标</param>
        /// <param name="length">长度</param>
        /// <returns></returns>
        public static string Reverse(this string str, int startIndex, int length)
        {
            return new string(str.ToArray().Reverse().ToArray(), startIndex, length);
        }

        /// <summary>
        /// 反转一个字符串
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="startIndex">开始下标</param>
        /// <returns></returns>
        public static string Reverse(this string str, int startIndex)
        {
            return str.Reverse(startIndex, str.Length - startIndex);
        }

        /// <summary>
        /// 反转一个字符串
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns></returns>
        public static string Reverse(this string str)
        {
            return str.Reverse(0, str.Length);
        }

        #endregion


    }
}
