﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FC.Math;

namespace FC.Test
{
    [TestClass]
    public class BigIntegerTest
    {
        /// <summary>
        /// 测试构造函数和ToString
        /// </summary>
        [TestMethod]
        public void TestNew()
        {
            BigInteger a = new BigInteger("12345");

            Assert.AreEqual("12345", a.ToString());
        }

        /// <summary>
        /// 测试加等 +=
        /// </summary>
        [TestMethod]
        public void TestAdd()
        {
            BigInteger a = new BigInteger("1");
            BigInteger b = new BigInteger("100");

            Assert.AreEqual("101", a.Add(b).ToString());
        }


    }
}
