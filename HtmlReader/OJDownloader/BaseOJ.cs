﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using FC.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using NSoup;
using NSoup.Nodes;

namespace OJDownloader
{
    /// <summary>
    /// 基础下载器
    /// </summary>
    public class BaseOJ
    {
        /// <summary>
        /// OJ系统地址
        /// </summary>
        public string Url { get; set; }
    }
}
