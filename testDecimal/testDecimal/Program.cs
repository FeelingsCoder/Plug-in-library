﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testDecimal
{
    class Program
    {
        static void Main(string[] args)
        {
            decimal[] ds = { 1m,2.0m,2.100m,2.000m,2.0001m,2.00001m,20.0m,};
            foreach (var d in ds)
            {
                Console.WriteLine(DecimalToString(d));
            }
        }
        private static string DecimalToString(decimal number)
        {
            string s = number.ToString();
            if (s.IndexOf('.') > 0)
            {
                for (int i = s.Count() - 1; i >= 0; i--)
                {
                    if (s[i] == '0')
                    {
                        s = s.Remove(i);
                    }
                    else if (s[i] == '.')
                    {
                        s = s.Remove(i);
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return s;
        }
    }
}
