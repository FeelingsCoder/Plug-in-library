﻿using FC.Data.Models;
using FC.Data.Models.AdministrativeDivision;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace FC.Data
{
    /// <summary>
    /// 数据库上下文
    /// </summary>
    public class FCDbContext : DbContext
    {
        /// <summary>
        /// 用户信息表
        /// </summary>
        public DbSet<UserInfo> Users { get; set; }

        /// <summary>
        /// 题目样例表
        /// </summary>
        public DbSet<ProblemExampleInfo> ProblemExamples { get; set; }

        /// <summary>
        /// 题目集表
        /// </summary>
        public DbSet<ProblemCollectionInfo> ProblemCollections { get; set; }

        /// <summary>
        /// 题目信息表
        /// </summary>
        public DbSet<ProblemInfo> Problems { get; set; }

        /// <summary>
        /// 用户提交记录表
        /// </summary>
        public DbSet<SubmitRecord> SubmitRecords { get; set; }

        /// <summary>
        /// VIP等级信息表
        /// </summary>
        public DbSet<VIPInfo> VIPs { get; set; }

        #region 中国行政区划

        /// <summary>
        /// 地区表
        /// </summary>
        public DbSet<AreaInfo> Areas { get; set; }

        /// <summary>
        /// 省 直辖市 自治区 表
        /// </summary>
        public DbSet<ProvinceInfo> Provinces { get; set; }

        /// <summary>
        /// 地级市 自治州
        /// </summary>
        public DbSet<CityInfo> Cities { get; set; }

        /// <summary>
        /// 县级市
        /// </summary>
        public DbSet<CountyInfo> Counties { get; set; }
        
        #endregion

        #region 加载实体映射

        /// <summary>
        /// 方法重写
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //加载实体映射
            //modelBuilder.Configurations.Add(new BsLineRepairRangeMap());
            
            modelBuilder.Properties().Where(p => p.Name == "RowVersion" || p.Name == "RowVersion".ToUpper()).Configure(p => p.IsConcurrencyToken());
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();//移除复数表名的契约

            //modelBuilder.Types().Configure(f => f.ToTable(("t_" + f.ClrType).ToUpper(Infran.GetDatabaseType()));  
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();  //表中都统一设置禁用一对多级联删除
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>(); //表中都统一设置禁用多对多级联删除
            base.OnModelCreating(modelBuilder);
        }
        #endregion

    }
}
