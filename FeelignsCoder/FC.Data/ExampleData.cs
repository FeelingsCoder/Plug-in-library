﻿using FC.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data
{
    /// <summary>
    /// 样例数据
    /// </summary>
    public class ExampleData : DropCreateDatabaseAlways<FCDbContext>
    {
        protected override void Seed(FCDbContext context)
        {
            #region 会员等级
            List<VIPInfo> vips = new List<VIPInfo>()
            {
                new VIPInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    SortId = 1,
                    Name = "普通用户",
                },
                new VIPInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    SortId = 2,
                    Name = "会员用户",
                },
                new VIPInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    SortId = 3,
                    Name = "超级会员用户",
                },
            };

            #endregion

            #region 用户信息
            List<UserInfo> users = new List<UserInfo>()
            {
                new UserInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    Password = "admin",
                    UserName = "admin",
                    PhoneNumber = "15263647272",
                    VIP = vips[2],
                }
            };
            #endregion

            #region

            List<ProblemCollectionInfo> collections = new List<ProblemCollectionInfo>()
            {
                new ProblemCollectionInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    Code = "PAT",
                    Name = "PAT 计算机程序设计能力考试",
                    State = "一直可用"
                },
                new ProblemCollectionInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    Code = "Lanqiao",
                    Name = "蓝桥杯全国软件和信息技术专业人才大赛",
                    State = "一直可用"
                }
            };

            #endregion

            #region 题目信息
            List<ProblemInfo> problems = new List<ProblemInfo>
            {
                new ProblemInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    No = 1001,
                    CollectionCode = "Lanqiao",
                    Keyword = "入门训练",
                    Title = "Fibonacci数列",
                    Description = @"Fibonacci数列的递推公式为：Fn=Fn-1+Fn-2，其中F1=F2=1。
当n比较大时，Fn也非常大，现在我们想知道，Fn除以10007的余数是多少。",
                    InputFormat = "输入包含一个整数n。",
                    OutputFormat = "输出一行，包含一个整数，表示Fn除以10007的余数。",
                    Tips = "在本题中，答案是要求Fn除以10007的余数，因此我们只要能算出这个余数即可，而不需要先计算出Fn的准确值，再将计算的结果除以10007取余数，直接计算余数往往比先算出原数再取余简单。",
                    Convention = "1 <= n <= 1,000,000。",
                    Score = 100,
                    PassCount = 0,
                    PassRate = 0,
                    SubmitCount = 0,

                    Author = "Lanqiao",
                    CodeLengthRestriction = 8000,
                    Link = "http://lx.lanqiao.cn/problem.page?gpid=T4",
                    MemoryLimit = 65536,
                    TestProcedure = "Standard",
                    TimeLimit = 1000
                },
                new ProblemInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    No = 1002,
                    CollectionCode = "Lanqiao",
                    Keyword = "入门训练",
                    Title = "圆的面积",
                    Description = @"给定圆的半径r，求圆的面积。
说明：
    在本题中，输入是一个整数，但是输出是一个实数。
    对于实数输出的问题，请一定看清楚实数输出的要求，比如本题中要求保留小数点后7位，则你的程序必须严格的输出7位小数，输出过多或者过少的小数位数都是不行的，都会被认为错误。实数输出的问题如果没有特别说明，舍入都是按四舍五入进行。",
                    InputFormat = "输入包含一个整数r，表示圆的半径。",
                    OutputFormat = "输出一行，包含一个实数，四舍五入保留小数点后7位，表示圆的面积。",
                    Tips = "本题对精度要求较高，请注意π的值应该取较精确的值。你可以使用常量来表示π，比如PI=3.14159265358979323，也可以使用数学公式来求π，比如PI=atan(1.0)*4。",
                    Convention = "1 <= r <= 10000。",
                    Score = 100,
                    PassCount = 0,
                    PassRate = 0,
                    SubmitCount = 0,

                    Author = "Lanqiao",
                    CodeLengthRestriction = 8000,
                    Link = "http://lx.lanqiao.cn/problem.page?gpid=T3",
                    MemoryLimit = 65536,
                    TestProcedure = "Standard",
                    TimeLimit = 1000

                },
                new ProblemInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    No = 1001,
                    CollectionCode = "PAT",
                    Keyword = "PAT (Basic Level) Practise",
                    Title = "跟奥巴马一起编程",
                    Description = "美国总统奥巴马不仅呼吁所有人都学习编程，甚至以身作则编写代码，成为美国历史上首位编写计算机代码的总统。2014年底，为庆祝“计算机科学教育周”正式启动，奥巴马编写了很简单的计算机代码：在屏幕上画一个正方形。现在你也跟他一起画吧！",
                    InputFormat = "输入在一行中给出正方形边长N（3<=N<=20）和组成正方形边的某种字符C，间隔一个空格。",
                    OutputFormat = "输出由给定字符C画出的正方形。但是注意到行间距比列间距大，所以为了让结果看上去更像正方形，我们输出的行数实际上是列数的50%（四舍五入取整）。",
                    Tips = "",
                    Convention = "",
                    Score = 15,
                    PassCount = 0,
                    PassRate = 0,
                    SubmitCount = 0,

                    Author = "CHEN, Yue",
                    CodeLengthRestriction = 8000,
                    Link = "https://www.patest.cn/contests/pat-b-practise/1036",
                    MemoryLimit = 65536,
                    TestProcedure = "Standard",
                    TimeLimit = 400

                },
                new ProblemInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    No = 1002,
                    CollectionCode = "PAT",
                    Keyword = "PAT (Basic Level) Practise",
                    Title = "在霍格沃茨找零钱",
                    Description = "如果你是哈利·波特迷，你会知道魔法世界有它自己的货币系统 —— 就如海格告诉哈利的：“十七个银西可(Sickle)兑一个加隆(Galleon)，二十九个纳特(Knut)兑一个西可，很容易。”现在，给定哈利应付的价钱P和他实付的钱A，你的任务是写一个程序来计算他应该被找的零钱。",
                    InputFormat = @"输入在1行中分别给出P和A，格式为“Galleon.Sickle.Knut”，其间用1个空格分隔。这里Galleon是[0, 107]区间内的整数，Sickle是[0, 17)区间内的整数，Knut是[0, 29)区间内的整数。",
                    OutputFormat = "在一行中用与输入同样的格式输出哈利应该被找的零钱。如果他没带够钱，那么输出的应该是负数。",
                    Tips = "",
                    Convention = "",
                    Score = 20,
                    PassCount = 0,
                    PassRate = 0,
                    SubmitCount = 0,

                    Author = "CHEN, Yue",
                    CodeLengthRestriction = 8000,
                    Link = "https://www.patest.cn/contests/pat-b-practise/1037",
                    MemoryLimit = 65536,
                    TestProcedure = "Standard",
                    TimeLimit = 400

                },
                new ProblemInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    No = 1003,
                    CollectionCode = "PAT",
                    Keyword = "PAT (Basic Level) Practise",
                    Title = "统计同成绩学生",
                    Description = "本题要求读入N名学生的成绩，将获得某一给定分数的学生人数输出。",
                    InputFormat = @"输入在第1行给出不超过105的正整数N，即学生总人数。
随后1行给出N名学生的百分制整数成绩，中间以空格分隔。
最后1行给出要查询的分数个数K（不超过N的正整数），随后是K个分数，中间以空格分隔。",
                    OutputFormat = "在一行中按查询顺序给出得分等于指定分数的学生人数，中间以空格分隔，但行末不得有多余空格。",
                    Tips = "",
                    Convention = "",
                    Score = 20,
                    PassCount = 0,
                    PassRate = 0,
                    SubmitCount = 0,

                    Author = "CHEN, Yue",
                    CodeLengthRestriction = 8000,
                    Link = "https://www.patest.cn/contests/pat-b-practise/1038",
                    MemoryLimit = 65536,
                    TestProcedure = "Standard",
                    TimeLimit = 250

                },
            };
            #endregion

            #region 题目样例
            List<ProblemExampleInfo> examples = new List<ProblemExampleInfo>()
            {
                new ProblemExampleInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    InputExample = "4",
                    OutputExample = "50.2654825",
                    Problem = problems[1],
                },
                new ProblemExampleInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    InputExample = "10",
                    OutputExample = "55",
                    Problem = problems[0],
                },
                new ProblemExampleInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    InputExample = "22",
                    OutputExample = "7704",
                    Problem = problems[0],
                },
                new ProblemExampleInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    InputExample = "10 a",
                    OutputExample = @"aaaaaaaaaa
a        a
a        a
a        a
aaaaaaaaaa",
                    Problem = problems[2],
                },
                new ProblemExampleInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    InputExample = "10.16.27 14.1.28",
                    OutputExample = "3.2.1",
                    Problem = problems[3],
                },
                new ProblemExampleInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    InputExample = "14.1.28 10.16.27",
                    OutputExample = "-3.2.1",
                    Problem = problems[3],
                },
                new ProblemExampleInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    InputExample = @"10
60 75 90 55 75 99 82 90 75 50
3 75 90 88",
                    OutputExample = "3 2 0",
                    Problem = problems[4],
                }
            };
            #endregion

            #region 用户提交记录表
            #endregion
            
            foreach (var user in users)
            {
                context.Users.Add(user);
            }
            foreach (var collection in collections)
            {
                context.ProblemCollections.Add(collection);
            }
            foreach (var problem in problems)
            {
                context.Problems.Add(problem);
            }
            foreach (var example in examples)
            {
                context.ProblemExamples.Add(example);
            }
            foreach (var vip in vips)
            {
                context.VIPs.Add(vip);
            }
        }
    }
}
