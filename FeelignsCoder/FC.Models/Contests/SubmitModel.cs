﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data.ViewModels.Contests
{
    /// <summary>
    /// 提交页面模型
    /// </summary>
    public class SubmitModel
    {
        /// <summary>
        /// 用户编号
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 题目编号
        /// </summary>
        public string ProblemId { get; set; }

        /// <summary>
        /// 试题名称
        /// </summary>
        public string ProbelmTitle { get; set; }
        
        /// <summary>
        /// 提交内容
        /// </summary>
        [Display(Name = "代码")]
        public string SubmitText { get; set; }
    }
}
