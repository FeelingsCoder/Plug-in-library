﻿using System.Collections.Generic;

namespace FC.Data.ViewModels.Contests
{
    /// <summary>
    /// 用于前台分页显示题目列表
    /// </summary>
    public class ProblemList
    {
        /// <summary>
        /// 页码
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// 总页数
        /// </summary>
        public int PageCount { get; set; }

        /// <summary>
        /// 问题列表
        /// </summary>
        public List<ProblemTitle> Problems { get; set; }
    }
}