﻿using System.ComponentModel.DataAnnotations;

namespace FC.Data.ViewModels.Contests
{
    /// <summary>
    /// 用于前端显示题目信息
    /// </summary>
    public class ProblemTitle
    {
        /// <summary>
        /// 题目状态
        /// </summary>
        [Display(Name = "状态")]
        public string State { get; set; }

        /// <summary>
        /// 题目状态样式
        /// </summary>
        public string StateCSS { get; set; }

        /// <summary>
        /// Id 唯一编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 显示编号
        /// </summary>
        [Display(Name = "编号")]
        public int No { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [Display(Name = "标题")]
        public string Title { get; set; }

        /// <summary>
        /// 关键字 分类
        /// </summary>
        [Display(Name = "分类")]
        public string Keyword { get; set; }

        /// <summary>
        /// 通过数量
        /// </summary>
        [Display(Name = "通过")]
        public int PassCount { get; set; }

        /// <summary>
        /// 提交数量
        /// </summary>
        [Display(Name = "提交")]
        public int SubmitCount { get; set; }

        /// <summary>
        /// 通过率
        /// </summary>
        [Display(Name = "通过率")]
        public decimal PassRate { get; set; }
    }
}