﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data.ViewModels.Contests
{
    /// <summary>
    /// 导出模型
    /// </summary>
    public class ExportProblemModel
    {
        /// <summary>
        /// Id 唯一编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 显示编号
        /// </summary>
        public string No { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 分值
        /// </summary>
        public string Score { get; set; }

        /// <summary>
        /// 题目集编码
        /// </summary>
        public string CollectionCode { get; set; }

        /// <summary>
        /// 题目描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 输入格式
        /// </summary>
        public string InputFormat { get; set; }

        /// <summary>
        /// 小提示
        /// </summary>
        public string Tips { get; set; }

        /// <summary>
        /// 输出格式
        /// </summary>
        public string OutputFormat { get; set; }

        /// <summary>
        /// 数据规模约定
        /// </summary>
        public string Convention { get; set; }

        /// <summary>
        /// 关键字 分类
        /// </summary>
        public string Keyword { get; set; }

        /// <summary>
        /// 通过数量
        /// </summary>
        public string PassCount { get; set; }

        /// <summary>
        /// 时间限制
        /// </summary>
        public string TimeLimit { get; set; }

        /// <summary>
        /// 内存限制
        /// </summary>
        public string MemoryLimit { get; set; }

        /// <summary>
        /// 代码长度限制
        /// </summary>
        public string CodeLengthRestriction { get; set; }

        /// <summary>
        /// 判题程序
        /// </summary>
        public string TestProcedure { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 原题连接
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// 提交数量
        /// </summary>
        public string SubmitCount { get; set; }

        /// <summary>
        /// 通过率
        /// </summary>
        public string PassRate { get; set; }
    }
}
