﻿
using System.ComponentModel.DataAnnotations;

namespace FC.Data.ViewModels.Contests
{
    /// <summary>
    /// 问题样例
    /// </summary>
    public class ProblemExample
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public string ProblemId { get; set; }
        
        /// <summary>
        /// 输入样例
        /// </summary>
        [Display(Name = "输入样例")]
        public string InputExample { get; set; }

        /// <summary>
        /// 输出样例
        /// </summary>
        [Display(Name = "输出样例")]
        public string OutputExample { get; set; }
    }
}
