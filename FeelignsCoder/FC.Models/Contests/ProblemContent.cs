﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FC.Data.ViewModels.Contests
{
    /// <summary>
    /// 用于显示题目信息
    /// </summary>
    public class ProblemContent
    {
        /// <summary>
        /// Id 唯一编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 显示编号
        /// </summary>
        [Display(Name = "显示编号")]
        public int No { get; set; }

        /// <summary>
        /// 题目集编号
        /// </summary>
        public string CollectionCode { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        [Display(Name = "关键字")]
        public string Keyword { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [Display(Name = "标题")]
        public string Title { get; set; }

        /// <summary>
        /// 分值
        /// </summary>
        [Display(Name = "分值")]
        public int Score { get; set; }

        /// <summary>
        /// 题目描述
        /// </summary>
        [Display(Name = "题目描述")]
        public string Description { get; set; }

        /// <summary>
        /// 输入格式
        /// </summary>
        [Display(Name = "输入格式")]
        public string InputFormat { get; set; }

        /// <summary>
        /// 输出格式
        /// </summary>
        [Display(Name = "输出格式")]
        public string OutputFormat { get; set; }

        /// <summary>
        /// 样例
        /// </summary>
        public List<ProblemExample> Examples { get; set; }

        /// <summary>
        /// 小提示
        /// </summary>
        [Display(Name = "小提示")]
        public string Tips { get; set; }
        
        /// <summary>
        /// 数据规模约定
        /// </summary>
        [Display(Name = "数据规模约定")]
        public string Convention { get; set; }
        
        /// <summary>
        /// 时间限制
        /// </summary>
        [Display(Name = "时间限制")]
        public int TimeLimit { get; set; }

        /// <summary>
        /// 内存限制
        /// </summary>
        [Display(Name = "内存限制")]
        public int MemoryLimit { get; set; }

        /// <summary>
        /// 代码长度限制
        /// </summary>
        [Display(Name = "代码长度限制")]
        public int CodeLengthRestriction { get; set; }

        /// <summary>
        /// 判题程序
        /// </summary>
        [Display(Name = "判题程序")]
        public string TestProcedure { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        [Display(Name = "作者")]
        public string Author { get; set; }

        /// <summary>
        /// 原题连接
        /// </summary>
        [Display(Name = "原题连接")]
        public string Link { get; set; }

    }
}
