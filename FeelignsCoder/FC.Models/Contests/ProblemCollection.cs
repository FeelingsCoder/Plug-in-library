﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data.ViewModels.Contests
{
    /// <summary>
    /// 用于前台显示题目集列表
    /// </summary>
    public class ProblemCollection
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 题目集编号
        /// </summary>
        [Display(Name = "题目集编号")]
        public string Code { get; set; }

        /// <summary>
        /// 题目集名称
        /// </summary>
        [Display(Name = "题目集名称")]
        public string Name { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Display(Name = "状态")]
        public string State { get; set; }
    }
}
