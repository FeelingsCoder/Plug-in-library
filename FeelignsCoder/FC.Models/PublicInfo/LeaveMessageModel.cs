﻿using System.ComponentModel.DataAnnotations;

namespace FC.Data.ViewModels.PublicInfo
{
    /// <summary>
    /// 留言板模型
    /// </summary>
    public class LeaveMessageModel
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [Display(Name = "姓名")]
        [Required(AllowEmptyStrings = false)]
        public string UserName { get; set; }
        /// <summary>
        /// 留言
        /// </summary>
        [Display(Name = "留言")]
        [Required(AllowEmptyStrings = false)]
        public string Message { get; set; }
    }
}
