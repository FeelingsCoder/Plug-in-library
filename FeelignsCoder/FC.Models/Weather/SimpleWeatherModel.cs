﻿using FC.Weather.Weatherdt.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data.ViewModels.Weather
{
    /// <summary>
    /// 简单天气数据模型
    /// </summary>
    public class SimpleWeatherModel
    {   
        /// <summary>
        /// 城市名称
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// 白天天气
        /// </summary>
        public string DayWeather { get; set; }

        /// <summary>
        /// 晚上天气
        /// </summary>
        public string NightWeather { get; set; }

        /// <summary>
        /// 白天温度
        /// </summary>
        public string DayTemperature { get; set; }

        /// <summary>
        /// 晚上温度
        /// </summary>
        public string NightTemperature { get; set; }
        
        /// <summary>
        /// 空气质量
        /// </summary>
        public string AirQuality { get; set; }

        /// <summary>
        /// PM 2.5指数
        /// </summary>
        public int PM2_5 { get; set; }

    }
}
