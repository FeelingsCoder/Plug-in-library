﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data.ViewModels.Weather
{
    /// <summary>
    /// 天气代码导入模型
    /// </summary>
    public class ImportWeatherCodeModel
    {
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 父编码
        /// </summary>
        public string ParentCode { get; set; }

        /// <summary>
        /// 城市名称
        /// </summary>
        public string Name { get; set; }
    }
}
