﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FC.Data.ViewModels.Account
{
    /// <summary>
    /// 注册页面表单
    /// </summary>
    public class RegisterForm
    {
        /// <summary>
        /// 用户名 唯一
        /// </summary>
        [Display(Name = "用户名")]
        [Remote("CheckUserName", "Account", HttpMethod = "GET")]
        [Required(AllowEmptyStrings = false)]
        [StringLength(25, MinimumLength = 5, ErrorMessage = "用户名长度应该在{2}-{1}位之间")]
        public string UserName { get; set; }

        /// <summary>
        /// 手机号 唯一
        /// </summary>
        [Display(Name = "手机号")]
        [Remote("CheckPhoneNumber", "Account", HttpMethod ="GET")]
        [Required(AllowEmptyStrings = false)]
        [RegularExpression("^[1][0-9]{10}$", ErrorMessage = "请输入11位手机号")]
        public string PhoneNumber { get; set; }


        /// <summary>
        /// 密码 
        /// </summary>
        [Display(Name = "密码")]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "请输入密码")]
        [StringLength(25, MinimumLength = 8, ErrorMessage = "密码长度应该在{2}和{1}位之间")]
        [RegularExpression("^.*[a-zA-Z].*$", ErrorMessage = "密码需要至少包含一个字母")]
        [System.ComponentModel.DataAnnotations.Compare("RepeatPassoword", ErrorMessage = "两次输入密码不一致")]
        public string Password { get; set; }

        /// <summary>
        /// 确认密码
        /// </summary>
        [Display(Name = "确认密码")]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "请再次输入密码")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "两次输入密码不一致")]
        public string RepeatPassoword { get; set; }
        
        ///DisplayName:指定当前字段显示的名称  
        ///StringLength：指定允许的长度，括号里的第一个参数是最大长度,
        ///第二个参数是最小长度。
        ///最后一个参数是指：单字段值的长度不符合第一第二参数规定时候，提示错误的内容  
        ///Required：表示当前字段是必填选项，当提交的表单缺少该值就引发验证错误。
        ///括号里的AllowEmptyStrings是： 获取或设置一个值，该值指示是否允许空字符串。
        ///单值为false的时候表示当前字段的值不能为空  
        ///Remote:允许利用服务器端的回调函数执行客户端的验证逻辑。
        ///说白了就是支持AJAX验证。括号里的CheckUserName是Action方法。
        ///Home是控制器 ,它会调用Home控制中的CheckUserName方法来查询你所输入的用户名是否已经存在，
        ///如果存在，则提示"用户名已经存在"  

    }
}