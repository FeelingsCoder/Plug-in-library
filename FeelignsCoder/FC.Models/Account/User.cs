﻿using System.ComponentModel.DataAnnotations;

namespace FC.Data.ViewModels.Account
{
    /// <summary>
    /// 用于前台显示用户信息
    /// </summary>
    public class User
    {
        /// <summary>
        /// 用户编号 唯一
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 手机号 唯一
        /// </summary>
        [Display(Name = "手机号")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// 用户名 唯一
        /// </summary>
        [Display(Name = "用户名")]
        public string UserName { get; set; }

        /// <summary>
        /// 头像路径
        /// </summary>
        public string IcoPath { get; set; }

        /// <summary>
        /// VIP等级编号
        /// </summary>
        public string VIP { get; set; }
    }
}
