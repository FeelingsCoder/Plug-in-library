﻿using System.ComponentModel.DataAnnotations;

namespace FC.Data.ViewModels.Account
{
    /// <summary>
    /// 登录页面表单
    /// </summary>
    public class LoginForm
    {
        /// <summary>
        /// 用户名 唯一
        /// </summary>
        [Display(Name = "用户名")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "请输入用户名")]
        public string UserName { get; set; }

        /// <summary>
        /// 密码 
        /// </summary>
        [Display(Name = "密码")]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "请输入密码")]
        public string Password { get; set; }
    }
}