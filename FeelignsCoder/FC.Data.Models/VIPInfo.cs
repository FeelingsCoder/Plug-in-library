﻿namespace FC.Data.Models
{
    /// <summary>
    /// VIP等级信息表
    /// </summary>
    public class VIPInfo
    {
        /// <summary>
        /// VIP等级Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// VIP等级标题
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// VIP等级排序
        /// </summary>
        public int? SortId { get; set; }

    }
}
