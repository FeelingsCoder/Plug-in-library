﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data.Models
{
    /// <summary>
    /// 问题样例
    /// </summary>
    public class ProblemExampleInfo
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public string ProblemId { get; set; }
        
        /// <summary>
        /// 输入样例
        /// </summary>
        public string InputExample { get; set; }

        /// <summary>
        /// 输出样例
        /// </summary>
        public string OutputExample { get; set; }

        /// <summary>
        /// 导航属性：题目信息
        /// </summary>
        public virtual ProblemInfo Problem { get; set; }
    }
}
