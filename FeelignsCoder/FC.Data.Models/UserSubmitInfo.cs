﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data.Models
{
    /// <summary>
    /// 用户提交信息
    /// </summary>
    public class UserSubmitInfo
    {
        /// <summary>
        /// 提交编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 用户编号
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 题目编号
        /// </summary>
        public string ProblemId { get; set; }

        /// <summary>
        /// 提交内容
        /// </summary>
        public string SubmitText { get; set; }

        /// <summary>
        /// 提交状态
        /// </summary>
        public string State { get; set; }
    }
}
