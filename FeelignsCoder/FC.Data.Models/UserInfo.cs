﻿using System.ComponentModel.DataAnnotations;

namespace FC.Data.Models
{
    /// <summary>
    /// 用户信息表
    /// </summary>
    public class UserInfo
    {
        /// <summary>
        /// 用户编号 唯一
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 手机号 唯一
        /// </summary>
        [Display(Name="手机号")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// 用户名 唯一
        /// </summary>
        [Display(Name= "用户名")]
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Display(Name="密码")]
        public string Password { get; set; }
        
        /// <summary>
        /// 头像路径
        /// </summary>
        public string IcoPath { get; set; }

        /// <summary>
        /// VIP等级编号
        /// </summary>
        public string VIPId { get; set; }

        /// <summary>
        /// 导航属性：VIP等级
        /// </summary>
        public virtual VIPInfo VIP { get; set; }
    }
}
