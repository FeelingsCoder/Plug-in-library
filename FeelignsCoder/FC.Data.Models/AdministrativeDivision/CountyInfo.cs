﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data.Models.AdministrativeDivision
{
    /// <summary>
    /// 中国行政区划 - 区/县
    /// </summary>
    public class CountyInfo
    {
        /// <summary>
        /// 编号 - 编号规则依据中国行政区划编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 城市编号
        /// </summary>
        public string CityId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 中国天气网代码
        /// </summary>
        public string WeatherCode { get; set; }
    }
}
