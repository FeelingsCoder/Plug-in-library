﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data.Models.AdministrativeDivision
{
    /// <summary>
    /// 中国行政区划 - 省
    /// </summary>
    public class ProvinceInfo
    {
        /// <summary>
        /// 编号 - 编号规则依据中国行政区划编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 地区编号
        /// </summary>
        public string AreaId { get; set; }

        /// <summary>
        /// 中国天气网代码
        /// </summary>
        public string WeatherCode { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 简称
        /// </summary>
        public string SimpleName { get; set; }

        /// <summary>
        /// 省会
        /// </summary>
        public string ProvincialCapital { get; set; }

        /// <summary>
        /// 面积 （万平方千米）
        /// </summary>
        public double? Area { get; set; }

        /// <summary>
        /// 人口 （万人）
        /// </summary>
        public double? Population { get; set; }

        /// <summary>
        /// 邮政编码
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// 政府驻地
        /// </summary>
        public string GovernmentResident { get; set; }
    }
}
