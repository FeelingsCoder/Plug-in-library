﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data.Models.AdministrativeDivision
{
    /// <summary>
    /// 中国行政区划 - 地区
    /// </summary>
    public class AreaInfo
    {
        /// <summary>
        /// 编号 - 唯一编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
