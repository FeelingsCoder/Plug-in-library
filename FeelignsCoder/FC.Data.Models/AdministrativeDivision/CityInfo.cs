﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data.Models.AdministrativeDivision
{
    /// <summary>
    /// 中国行政区划 - 市
    /// </summary>
    public class CityInfo
    {
        /// <summary>
        /// 编号 - 编号规则依据中国行政区划编号
        /// </summary>
        public string Id { get; set; }
        
        /// <summary>
        /// 省级编号
        /// </summary>
        public string ProvinceId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 中国天气网代码
        /// </summary>
        public string WeatherCode { get; set; }
    }
}
