﻿
namespace FC.Data.Models
{
    /// <summary>
    /// 用户提交记录表
    /// </summary>
    public class SubmitRecord
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public string ProblemId { get; set; }
        
        /// <summary>
        /// 题目状态
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// 导航属性：用户信息
        /// </summary>
        public UserInfo User { get; set; }

        /// <summary>
        /// 导航属性：题目信息
        /// </summary>
        public ProblemInfo Problem { get; set; }
    }
}
