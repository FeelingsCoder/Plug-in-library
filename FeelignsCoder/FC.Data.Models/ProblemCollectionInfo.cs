﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Data.Models
{
    /// <summary>
    /// 题目集
    /// </summary>
    public class ProblemCollectionInfo
    {
        /// <summary>
        /// 编号 Guid
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 题目集编码
        /// </summary>
        public string Code { set; get; }

        /// <summary>
        /// 题目集名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public string State { get; set; }
    }
}
