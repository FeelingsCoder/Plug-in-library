﻿
using System.Collections.Generic;

namespace FC.Data.Models
{
    /// <summary>
    /// 题目信息表
    /// </summary>
    public class ProblemInfo
    {
        /// <summary>
        /// Id 唯一编号
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 显示编号
        /// </summary>
        public int No { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 分值
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// 题目集编码
        /// </summary>
        public string CollectionCode { get; set; }

        /// <summary>
        /// 题目描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 输入格式
        /// </summary>
        public string InputFormat { get; set; }

        /// <summary>
        /// 小提示
        /// </summary>
        public string Tips { get; set; }

        /// <summary>
        /// 输出格式
        /// </summary>
        public string OutputFormat { get; set; }

        /// <summary>
        /// 数据规模约定
        /// </summary>
        public string Convention { get; set; }

        /// <summary>
        /// 关键字 分类
        /// </summary>
        public string Keyword { get; set; }

        /// <summary>
        /// 通过数量
        /// </summary>
        public int PassCount { get; set; }
        
        /// <summary>
        /// 时间限制
        /// </summary>
        public int TimeLimit { get; set; }

        /// <summary>
        /// 内存限制
        /// </summary>
        public int MemoryLimit { get; set; }

        /// <summary>
        /// 代码长度限制
        /// </summary>
        public int CodeLengthRestriction { get; set; }

        /// <summary>
        /// 判题程序
        /// </summary>
        public string TestProcedure { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 原题连接
        /// </summary>
        public string Link { get; set; }
        
        /// <summary>
        /// 提交数量
        /// </summary>
        public int SubmitCount { get; set; }

        /// <summary>
        /// 通过率
        /// </summary>
        public decimal PassRate { get; set; }

        /// <summary>
        /// 导航属性：样例信息
        /// </summary>
        public virtual List<ProblemExampleInfo> Examples { get; set; }
    }
}
