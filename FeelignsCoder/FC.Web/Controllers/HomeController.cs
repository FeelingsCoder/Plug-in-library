﻿using FC.Data.ViewModels.PublicInfo;
using FC.Data.ViewModels.Weather;
using FC.IService;
using FC.Service;
using FC.Weather.PM25;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FC.Web.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// 天气服务
        /// </summary>
        IWeatherService weatherService = new WeatherService();

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 留言
        /// </summary>
        /// <returns></returns>
        public ActionResult LeaveMessage()
        {
            return View();
        }

        /// <summary>
        /// 留言
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LeaveMessage(LeaveMessageModel model)
        {
            return RedirectToAction("Index");
        }

        /// <summary>
        /// 获取简单天气数据 导航栏天气信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSimpleWeather()
        {
            SimpleWeatherModel weather = null;
            if (Session["Weather"] == null)
            {
                weather = weatherService.GetSimpleWeather("101020100");
                Session.Add("Weather", weather);
            }
            else weather = Session["Weather"] as SimpleWeatherModel;
            weather.CityName = "上海";
            return Json(weather, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取PM2.5指数
        /// </summary>
        /// <returns></returns>
        public JsonResult GetPM2_5()
        {
            PM2_5 pm25 = null;
            if (Session["PM2_5"] == null)
            {
                pm25 = weatherService.GetPM2_5("上海");
                Session.Add("PM2_5", pm25);
            }
            else pm25 = Session["PM2_5"] as PM2_5;

            return Json(pm25, JsonRequestBehavior.AllowGet);
        }
    }
}