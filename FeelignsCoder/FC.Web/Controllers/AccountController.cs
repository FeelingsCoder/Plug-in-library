﻿using FC.Data.ViewModels.Account;
using FC.IService;
using FC.Service;
using FC.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FC.Web.Controllers
{
    public class AccountController : Controller
    {
        /// <summary>
        /// 账户服务
        /// </summary>
        IAccountService accountService = new AccountService();

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 登录界面
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            if (Session["User"] != null) return RedirectToAction("Index", "Home");
            return View();
        }

        /// <summary>
        /// 登录页面数据提交
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginForm model)
        {
            if (ModelState.IsValid)
            {
                User user = accountService.Login(model);
                if (user != null)
                {
                    Session.Add("User", user);
                    if (Session["CurrentUrl"] == null)
                    {
                        return Redirect("~/Home");
                    }
                    else
                    {
                        return Redirect(Session["CurrentUrl"].ToString());
                    }
                }
                else
                {
                    return View();
                }
            }
            return View();
        }

        /// <summary>
        /// 注销
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            Session.Remove("User");
            return RedirectToAction("Login");
        }

        /// <summary>
        /// 注册页面
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// 注册页面提交
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterForm model)
        {
            //if (ModelState.IsValid)
            //{
            //    var result = accountService.Register(model);
            //    if(result.state == "ok")
            //    {
            //        return RedirectToAction("Login");
            //    }
            //}
            //model.Password = model.RepeatPassoword = "";
            return View(model);
        }

        /// <summary>
        /// 检查手机号是否已注册
        /// </summary>
        /// <param name="PhoneNumber">用户注册手机号</param>
        /// <returns></returns>
        public JsonResult CheckPhoneNumber(string PhoneNumber)
        {
            var result = accountService.CheckPhoneNumber(PhoneNumber);
            if(result.state == "ok")
                return Json(true, JsonRequestBehavior.AllowGet);
            else
                return Json(result.message, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 检查用户名是否已注册
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public JsonResult CheckUserName(string UserName)
        {
            var result = accountService.CheckUserName(UserName);
            if (result.state == "ok")
                return Json(true, JsonRequestBehavior.AllowGet);
            else
                return Json(result.message, JsonRequestBehavior.AllowGet);
        }
    }
}