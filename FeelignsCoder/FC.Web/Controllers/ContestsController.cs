﻿using FC.Data.ViewModels.Contests;
using FC.IService;
using FC.Service;
using FC.Web.Fillters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FC.Web.Controllers
{
    [LoginFillter]
    public class ContestsController : Controller
    {
        IContestsService service = new ContestsService();

        // GET: Contests
        public ActionResult Index()
        {
            ViewBag.ContestsTitle = "Home";
            return View();
        }

        /// <summary>
        /// 题目集
        /// </summary>
        /// <returns></returns>
        public ActionResult Collection()
        {
            // 模型 ProblemCollectionList
            var list = service.GetCollections();
            return View(list);
        }

        /// <summary>
        /// 题目列表页
        /// </summary>
        /// <param name="first">题目集编号</param>
        /// <param name="page">页码</param>
        /// <returns></returns>
        public ActionResult List(string first, int page = 1)
        {
            ViewBag.Collection = first;
            ViewBag.CollectionName = service.GetCollectionName(first);
            
            ProblemList list = service.GetProblemList(first, page);

            return View(list);
        }

        /// <summary>
        /// 题目详情页
        /// </summary>
        /// <param name="first">题目集编号</param>
        /// <param name="second">题目编号</param>
        /// <returns></returns>
        public ActionResult Problems(string first, int second)
        {
            ViewBag.Collection = first;

            ProblemContent problem = service.GetProblemContent(first, second);

            return View(problem);
        }

        /// <summary>
        /// 提交列表
        /// </summary>
        /// <param name="first">题目集编号</param>
        /// <param name="page">页码</param>
        /// <returns></returns>
        public ActionResult SubmitList(string first, int page = 1)
        {
            ViewBag.Collection = first;

            return View();
        }

        /// <summary>
        /// 排名列表
        /// </summary>
        /// <param name="first">题目集编号</param>
        /// <returns></returns>
        public ActionResult Power(string first)
        {
            ViewBag.Collection = first;

            return View();
        }

        /// <summary>
        /// 提交页面
        /// </summary>
        /// <param name="first">题目集编号</param>
        /// <param name="second">题目编号</param>
        /// <returns></returns>
        public ActionResult Submit(string first, int second)
        {
            ViewBag.Collection = first;
            return View();
        }

        /// <summary>
        /// 提交页面
        /// </summary>
        /// <param name="model">提交信息</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Submit(SubmitModel model)
        {
            return Json("");
        }
    }
}