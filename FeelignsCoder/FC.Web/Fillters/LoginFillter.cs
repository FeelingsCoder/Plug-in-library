﻿using FC.Data.ViewModels.Account;
using System.Web;
using System.Web.Mvc;

namespace FC.Web.Fillters
{
    /// <summary>
    /// 登录验证
    /// </summary>
    public class LoginFillter : FilterAttribute, IActionFilter
    {
        /// <summary>
        /// 执行action后执行这个方法 比如做操作日志  
        /// </summary>
        /// <param name="filterContext"></param>
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }

        /// <summary>
        /// 执行action前执行这个方法,比如做身份验证 
        /// </summary>
        /// <param name="filterContext"></param>
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var sessionUser = HttpContext.Current.Session["User"];

            if (sessionUser == null || !(sessionUser is User))
            {
                HttpContext.Current.Session["CurrentUrl"] =
                    filterContext.RequestContext.HttpContext.Request.RawUrl;

                filterContext.Result = new RedirectToRouteResult
                    (new System.Web.Routing.RouteValueDictionary
                    (
                        new { controller = "Account", action = "Login" }
                    ));
                return;
            }
            else
            {
                User currentUser = (User)sessionUser;
            }
            return;
        }
    }
}