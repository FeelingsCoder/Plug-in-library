﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Weather.PM25
{
    /// <summary>
    /// PM2.5 数据模型 （PM25.in）
    /// </summary>
    public class PM2_5
    {
        /// <summary>
        /// 空气质量指数(AQI)，即air quality index，是定量描述空气质量状况的无纲量指数
        /// </summary>
        public string aqi { get; set; }

        /// <summary>
        /// 城市名称
        /// </summary>
        public string area { get; set; }

        /// <summary>
        /// 颗粒物（粒径小于等于2.5μm）1小时平均
        /// </summary>
        public string pm2_5 { get; set; }

        /// <summary>
        /// 颗粒物（粒径小于等于2.5μm）24小时滑动平均
        /// </summary>
        public string pm2_5_24h { get; set; } 

        /// <summary>
        /// 监测点名称
        /// </summary>
        public string position_name { get; set; }

        /// <summary>
        /// 首要污染物
        /// </summary>
        public string primary_pollutant { get; set; } 

        /// <summary>
        /// 空气质量指数类别，有“优、良、轻度污染、中度污染、重度污染、严重污染”6类
        /// </summary>
        public string quality { get; set; } 

        /// <summary>
        /// 监测点编码
        /// </summary>
        public string station_code { get; set; }

        /// <summary>
        /// 数据发布的时间
        /// </summary>
        public string time_point { get; set; }

        /// <summary>
        /// 错误消息
        /// </summary>
        public string error { get; set; }
    }
}
