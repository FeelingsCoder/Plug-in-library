﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Weather.Weatherdt
{
    public static class Utils
    {
        /// <summary>
        /// 格式化预报
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static string FormatForecast(this string json)
        {
            json = json.Replace("24h", "Forecast24Hours");
            json = json.Replace("101020100", "Site");
            json = json.Replace("1001001", "Datas");
            json = json.Replace("000", "UpdateTime");
            json = json.Replace("001", "DayWeather");
            json = json.Replace("002", "NightWeather");
            json = json.Replace("003", "DayTemperature");
            json = json.Replace("004", "NightTemperature");
            json = json.Replace("005", "DayWindPower");
            json = json.Replace("006", "NightWindPower");
            json = json.Replace("007", "DayWindDirection");
            json = json.Replace("008", "NightWindDirection");
            //json = json.Replace("'", "\"");
            return json;
        }
    }
}
