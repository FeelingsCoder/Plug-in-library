﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Weather.Weatherdt.Enums
{
    /// <summary>
    /// 天气情况枚举
    /// </summary>
    public enum WeatherEnum
    {
        /// <summary>
        /// 晴
        /// </summary>
        [Description("晴")]
        Sunny,
        /// <summary>
        /// 多云
        /// </summary>
        [Description("多云")]
        Cloudy,
        /// <summary>
        /// 阴
        /// </summary>
        [Description("阴")]
        Overcast,
        /// <summary>
        /// 阵雨
        /// </summary>
        [Description("阵雨")]
        Shower,
        /// <summary>
        /// 雷阵雨 
        /// </summary>
        [Description("雷阵雨")]
        Thundershower,
        /// <summary>
        /// 雷阵雨伴有冰雹
        /// </summary>
        [Description("雷阵雨伴有冰雹")]
        ThundershowerWithHail,
        /// <summary>
        /// 雨夹雪
        /// </summary>
        [Description("雨夹雪")]
        Sleet,
        /// <summary>
        /// 小雨
        /// </summary>
        [Description("小雨")]
        LightRain,
        /// <summary>
        /// 中雨
        /// </summary>
        [Description("中雨")]
        ModerateRain,
        /// <summary>
        /// 大雨
        /// </summary>
        [Description("大雨")]
        HeavyRain,
        /// <summary>
        /// 暴雨
        /// </summary>
        [Description("暴雨")]
        Storm,
        /// <summary>
        /// 大暴雨
        /// </summary>
        [Description("大暴雨")]
        HeavyStorm,
        /// <summary>
        /// 特大暴雨
        /// </summary>
        [Description("特大暴雨")]
        SevereStorm,
        /// <summary>
        /// 阵雪
        /// </summary>
        [Description("阵雪")]
        SnowFlurry,
        /// <summary>
        /// 小雪
        /// </summary>
        [Description("小雪")]
        LightSnow,
        /// <summary>
        /// 中雪
        /// </summary>
        [Description("中雪")]
        ModerateSnow,
        /// <summary>
        /// 大雪
        /// </summary>
        [Description("大雪")]
        HeavySnow,
        /// <summary>
        /// 暴雪
        /// </summary>
        [Description("暴雪")]
        Snowstorm,
        /// <summary>
        /// 雾
        /// </summary>
        [Description("雾")]
        Foggy,
        /// <summary>
        /// 冻雨
        /// </summary>
        [Description("冻雨")]
        IceRain,
        /// <summary>
        /// 沙尘暴
        /// </summary>
        [Description("沙尘暴")]
        Duststorm,
        /// <summary>
        ///小到中雨
        /// </summary>
        [Description("小到中雨")]
        LightToModerateRain,
        /// <summary>
        ///中到大雨
        /// </summary>
        [Description("中到大雨")]
        ModerateToHeavyRain,
        /// <summary>
        ///大到暴雨
        /// </summary>
        [Description("大到暴雨")]
        HeavyRainToStorm,
        /// <summary>
        ///暴雨到大暴雨
        /// </summary>
        [Description("暴雨到大暴雨")]
        StormToHeavyStorm,
        /// <summary>
        ///大暴雨到特大暴雨
        /// </summary>
        [Description("大暴雨到特大暴雨")]
        HeavyToSevereStorm,
        /// <summary>
        ///小到中雪
        /// </summary>
        [Description("小到中雪")]
        LightSoModerateSnow,
        /// <summary>
        ///中到大雪
        /// </summary>
        [Description("中到大雪")]
        ModerateToHeavySnow,
        /// <summary>
        ///大到暴雪
        /// </summary>
        [Description("大到暴雪")]
        HeavySnowToSnowstorm,
        /// <summary>
        ///浮尘
        /// </summary>
        [Description("浮尘")]
        Dust,
        /// <summary>
        ///扬沙
        /// </summary>
        [Description("扬沙")]
        Sand,
        /// <summary>
        ///强沙尘暴
        /// </summary>
        [Description("强沙尘暴")]
        Sandstorm,
        /// <summary>
        ///浓雾
        /// </summary>
        [Description("浓雾")]
        HeavyFog = 32,
        /// <summary>
        ///强浓雾
        /// </summary>
        [Description("强浓雾")]
        StrongFog = 49,
        /// <summary>
        ///霾
        /// </summary>
        [Description("霾")]
        Haze = 53,
        /// <summary>
        ///中度霾
        /// </summary>
        [Description("中度霾")]
        ModerateHaze,
        /// <summary>
        ///重度霾
        /// </summary>
        [Description("重度霾")]
        HeavyHaze,
        /// <summary>
        ///严重霾
        /// </summary>
        [Description("严重霾")]
        SevereHaze,
        /// <summary>
        ///大雾
        /// </summary>
        [Description("大雾")]
        DenseFog,
        /// <summary>
        ///特强浓雾
        /// </summary>
        [Description("特强浓雾")]
        ExtraHeavyFog,
        /// <summary>
        ///无
        /// </summary>
        [Description("未知天气")]
        Unknown = 99,
        /// <summary>
        ///雨
        /// </summary>
        [Description("雨")]
        Rain = 301,
        /// <summary>
        ///雪
        /// </summary>
        [Description("雪")]
        Snow = 302
    }
}
