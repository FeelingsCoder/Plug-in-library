﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Weather.Weatherdt.Models
{
    /// <summary>
    /// 中国天气网 气象数据集市 预报模型
    /// </summary>
    public class Forecast
    {
        /// <summary>
        /// 24小数数据
        /// </summary>
        public Forecast24Hours Forecast24Hours { get; set; }
    }

    /// <summary>
    /// 中国天气网 气象数据集市 预报时间模型
    /// </summary>
    public class Forecast24Hours
    {
        /// <summary>
        /// 站点
        /// </summary>
        public ForecastSite Site { get; set; }
    }

    /// <summary>
    /// 中国天气网 气象数据集市 预报站点模型
    /// </summary>
    public class ForecastSite
    {
        /// <summary>
        /// 数据
        /// </summary>
        public List<ForecastData> Datas { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public string UpdateTime { get; set; }
    }

    /// <summary>
    /// 中国天气网 气象数据集市 预报数据模型
    /// </summary>
    public class ForecastData
    {
        /// <summary>
        /// 白天天气现象编码
        /// </summary>
        public string DayWeather { get; set; }
        /// <summary>
        /// 晚上天气现象编码
        /// </summary>
        public string NightWeather { get; set; }
        /// <summary>
        /// 白天温度
        /// </summary>
        public string DayTemperature { get; set; }
        /// <summary>
        /// 晚上温度
        /// </summary>
        public string NightTemperature { get; set; }
        /// <summary>
        /// 白天风力
        /// </summary>
        public string DayWindPower { get; set; }
        /// <summary>
        /// 晚上风力
        /// </summary>
        public string NightWindPower { get; set; }
        /// <summary>
        /// 白天风向方向
        /// </summary>
        public string DayWindDirection { get; set; }
        /// <summary>
        /// 晚上风向方向
        /// </summary>
        public string NightWindDirection { get; set; }
    }
}
