﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Weather.Weatherdt.Models
{
    public class WeatherModel
    {
        /// <summary>
        /// 预报
        /// </summary>
        public Forecast forecast { get; set; }
    }
}
