﻿using FC.Data;
using FC.IService;
using FC.Data.ViewModels.Account;
using System.Linq;
using System;
using FC.Utils;

namespace FC.Service
{
    public class AccountService : IAccountService
    {
        /// <summary>
        /// 数据库上下文
        /// </summary>
        FCDbContext context = new FCDbContext();
        
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public User Login(LoginForm user)
        {
            var result =   
                from u in context.Users
                where (u.UserName == user.UserName || u.PhoneNumber == user.UserName) && u.Password == user.Password
                select new User {
                    Id = u.Id,
                    IcoPath = u.IcoPath,
                    PhoneNumber = u.PhoneNumber,
                    UserName = u.UserName,
                    VIP = u.VIP.Name,
                };
            return result.FirstOrDefault();
        }

        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Result Register(RegisterForm user)
        {
            var vip =
                from v in context.VIPs
                where v.Name == "普通用户"
                select v.Id;

            Result result = new Result() { state = "ok" };
            using (var transaction = context.Database.BeginTransaction())
            {
                context.Users.Add(new Data.Models.UserInfo()
                {
                    Id = Guid.NewGuid().ToString(),
                    IcoPath = "",
                    Password = user.Password,
                    PhoneNumber = user.PhoneNumber,
                    UserName = user.UserName,
                    VIPId = vip.FirstOrDefault()
                });
                try
                {
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    result.state = "error";
                    result.message = e.Message;
                    transaction.Rollback();
                }
            }
            return result;
        }
        
        /// <summary>
        /// 检查手机号是否已注册
        /// </summary>
        /// <param name="phoneNumber">用户注册手机号</param>
        /// <returns></returns>
        public Result CheckPhoneNumber(string phoneNumber)
        {
            Result result = context.Users.Any(p => p.PhoneNumber == phoneNumber) ?
                new Result() { state = "error", message = "手机号已被占用" }
                : new Result() { state = "ok", message = "" };
            return result;
        }

        /// <summary>
        /// 检查用户名是否已注册
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public Result CheckUserName(string userName)
        {
            Result result = context.Users.Any(p => p.UserName == userName) ?
                new Result() { state = "error", message = "用户名已存在" }
                : new Result() { state = "ok", message = "" };
            return result;
        }
    }
}
