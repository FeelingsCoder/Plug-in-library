﻿using FC.Data;
using FC.Data.ViewModels.Weather;
using FC.IService;
using FC.Utils;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FC.Service
{
    /// <summary>
    /// 公共信息服务
    /// </summary>
    public class PublicService : IPublicService
    {
        /// <summary>
        /// 数据库上下文
        /// </summary>
        FCDbContext context = new FCDbContext();

        public IWorkbook GetPublicInfo()
        {
            IWorkbook workbook = new HSSFWorkbook();
            workbook.CreateSheet(context.Areas.ToList(), "Areas");
            workbook.CreateSheet(context.Cities.ToList(), "Cities");
            workbook.CreateSheet(context.Counties.ToList(), "Counties");
            return workbook;
        }

        /// <summary>
        /// 导入天气代码
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public Result ImportWeatherCode(HttpPostedFileBase file, out IWorkbook resultWorkbook )
        {
            resultWorkbook = null;
            Result result = new Result() { state = "ok" , message = ""};
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var counties = context.Counties.ToList();
                    var cities = context.Cities.ToList();

                    IWorkbook workbook = file.CreateWorkbook();
                    ISheet sheet = workbook.GetSheetAt(0);

                    resultWorkbook = new HSSFWorkbook();
                    if (sheet != null)
                    {
                        int i = 1;
                        List<ImportWeatherCodeModel> list = sheet.ToList<ImportWeatherCodeModel>();
                        List<ImportWeatherCodeModel> citylist = new List<ImportWeatherCodeModel>();
                        List<ImportWeatherCodeModel> countylist = new List<ImportWeatherCodeModel>();
                        List<ImportWeatherCodeModel> errorlist = new List<ImportWeatherCodeModel>();

                        foreach (var item in list)
                        {
                            var county = (from c in counties where c.Name.StartsWith(item.Name) select c).ToList();
                            var city = (from c in cities where c.Name.StartsWith(item.Name) select c).ToList();
                            if (county.Count == 1)
                            { // 找到唯一匹配
                                county[0].WeatherCode = item.Code;
                                context.Counties.AddOrUpdate(county[0]);
                                countylist.Add(item);
                            }
                            else if (city.Count == 1)
                            {
                                city[0].WeatherCode = item.Code;
                                context.Cities.AddOrUpdate(city[0]);
                                citylist.Add(item);
                            }
                            else
                            {
                                result.message += $"{i} : {item.Name} - {item.Code} \n ==> city {city.Count} 个 匹配\n ==> 在county中找到 {county.Count} 个 匹配\n";
                                errorlist.Add(item);
                            }
                            i++;
                        }
                        resultWorkbook.CreateSheet(citylist, "city");
                        resultWorkbook.CreateSheet(countylist, "county");
                        resultWorkbook.CreateSheet(errorlist, "error");
                    }
                    
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    result.message = e.Message;
                    result.state = "error";
                    transaction.Rollback();
                }
            }
            return result;
        }
    }
}
