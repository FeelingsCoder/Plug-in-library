﻿using System;
using FC.Data.ViewModels.Weather;
using FC.IService;
using FC.Utils;
using FC.Weather.PM25;
using FC.Weather.Weatherdt;
using FC.Weather.Weatherdt.Enums;
using FC.Weather.Weatherdt.Models;


namespace FC.Service
{
    /// <summary>
    /// 天气服务接口实现
    /// Key 5c3e798524e1335a9b4304ad39bc20b4 有效期 2017-1-12 ~ 2017-1-19
    /// </summary>
    public class WeatherService : IWeatherService
    {
        /// <summary>
        /// 
        /// </summary>
        public string KEY { get; } = "5c3e798524e1335a9b4304ad39bc20b4";

        public PM2_5 GetPM2_5(string city)
        {
            PM25inWeather pm25in = new PM25inWeather();
            return pm25in.GetPM2_5(city);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="area">地区,(101)全国</param>
        /// <param name="type">天气数据类型 forecast(预报)|observe(实况)|alarm(预警)|air(空气质量)|index(指数)</param>
        /// <returns></returns>
        public SimpleWeatherModel GetSimpleWeather(string area, string type = "forecast")
        {
            WeatherdtWeather weatherd = new WeatherdtWeather();
            WeatherModel weather = weatherd.GetWeather(area, KEY, type);

            string dayWeather = weather.forecast.Forecast24Hours.Site.Datas[0].DayWeather;
            string nightWeather = weather.forecast.Forecast24Hours.Site.Datas[0].NightWeather;

            SimpleWeatherModel model = new SimpleWeatherModel()
            {
                DayTemperature = weather.forecast.Forecast24Hours.Site.Datas[0].DayTemperature + "℃",
                NightTemperature = weather.forecast.Forecast24Hours.Site.Datas[0].NightTemperature + "℃",
                DayWeather = ((WeatherEnum)(string.IsNullOrWhiteSpace(dayWeather) ? 0 : int.Parse(dayWeather))).GetDescription(),
                NightWeather = dayWeather == nightWeather ? "" : ((WeatherEnum)(string.IsNullOrWhiteSpace(nightWeather) ? 0 : int.Parse(nightWeather))).GetDescription(),
            };
            return model;
        }
    }
}
