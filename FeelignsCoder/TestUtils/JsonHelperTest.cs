﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FC.Utils;
using FC.Weather.Weatherdt;
using FC.Weather.Weatherdt.Models;

namespace TestUtils
{
    [TestClass]
    public class JsonHelperTest
    {
        [TestMethod]
        public void TestToClass()
        {
            //string json = "{\"006\":\"0\",\"008\":\"6\",\"007\":\"\",\"003\":\"\",\"004\":\"3\",\"001\":\"\",\"005\":\"\",\"002\":\"01\"}";
            string json = "{'forecast':{'24h':{'101020100':{'1001001':[{'006':'0','008':'6','007':'','003':'','004':'3','001':'','005':'','002':'01'},{'006':'0','008':'0','007':'7','003':'11','004':'3','001':'00','005':'0','002':'00'},{'006':'0','008':'8','007':'1','003':'8','004':'2','001':'00','005':'0','002':'01'}],'000':'201701121800'}}}}";

            json = json.FormatForecast();

            var r = json.ToClass<WeatherModel>();
        }
    }
}
