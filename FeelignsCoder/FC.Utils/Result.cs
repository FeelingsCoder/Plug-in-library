﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Utils
{
    /// <summary>
    /// 表示程序执行状态的类，用于Json返回
    /// </summary>
    public class Result
    {
        /// <summary>
        /// 状态
        /// </summary>
        public string state { get; set; }
        /// <summary>
        /// 返回消息
        /// </summary>
        public string message { get; set; }
    
    }
}
