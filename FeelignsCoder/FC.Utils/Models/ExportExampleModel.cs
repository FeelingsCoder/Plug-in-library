﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Utils.Models
{
    /// <summary>
    /// 样例导出模型
    /// </summary>
    public class ExportExampleModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public string ProblemId { get; set; }

        /// <summary>
        /// 输入样例
        /// </summary>
        public string InputExample { get; set; }

        /// <summary>
        /// 输出样例
        /// </summary>
        public string OutputExample { get; set; }
    }
}
