﻿using FC.Utils.Models;
using NSoup;
using NSoup.Nodes;
using NSoup.Select;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.Utils
{
    /// <summary>
    /// 在线测评系统
    /// </summary>
    public class HtmlHelper
    {
        public void Nyoj()
        {
            string baseurl = "http://acm.nyist.net/JudgeOnline/problem.php?pid=";
            //for(int id = 1; id <= 1000; id++)
            //{
                int id = 1;
                string url = baseurl + id;
                try
                {
                    Document document = NSoupClient.Parse(new Uri(url), 10000);
                    ExportProblemModel problem = new ExportProblemModel();
                    ExportExampleModel example = new ExportExampleModel();

                    problem.CollectionCode = "NYOJ";
                    problem.Id = "A5AFA617-B1C3-474F-9370-D4ECB6F" + (10000 + id).ToString();
                    problem.No = (1000 + id).ToString();
                    problem.Link = url;
                    problem.Title = document.Select("div.problem-display h2").First().Val();
                    problem.Description = document.Select("DL.problem-display dd")[0].Val();
                    problem.InputFormat = document.Select("DL.problem-display dd")[1].Val();
                    problem.OutputFormat = document.Select("DL.problem-display dd")[2].Val();
                    problem.Keyword = document.Select("DL.problem-display dd")[5].Val();
                    problem.Author = document.Select("DL.problem-display dd")[6].Val();
                    problem.MemoryLimit = document.Select("div.problem-ins span")[1].Val();
                    problem.TimeLimit = document.Select("div.problem-ins span")[0].Val();
                    problem.PassCount = "0";
                    problem.PassRate = "0";
                    problem.Score = "100";
                    problem.SubmitCount = "0";
                    problem.TestProcedure = "Standard";
                    problem.Tips = "";
                    problem.Convention = "";
                    problem.CodeLengthRestriction = "";

                    example.Id = "B5BFB637-A1C3-4A4F-9A70-D4ECB6F" + (10000 + id).ToString();
                    example.ProblemId = problem.Id;
                    example.InputExample = document.Select("DL.problem-display dd")[3].Val();
                    example.OutputExample = document.Select("DL.problem-display dd")[4].Val();

                }
                catch (Exception e)
                {
                throw e;
                }
            //}
        }
    }
}
