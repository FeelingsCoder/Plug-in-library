﻿using FC.Utils;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FC.IService
{
    /// <summary>
    /// 公共信息服务接口
    /// </summary>
    public interface IPublicService
    {
        Result ImportWeatherCode(HttpPostedFileBase file, out IWorkbook resultWorkbook);

        IWorkbook GetPublicInfo();
    }
}
