﻿using FC.Data.ViewModels.Contests;
using FC.Utils;
using NPOI.SS.UserModel;
using System.Collections.Generic;
using System.Web;

namespace FC.IService
{
    public interface IContestsService
    {
        /// <summary>
        /// 根据类型和页码获取题目列表
        /// </summary>
        /// <param name="problemType">题目类型</param>
        /// <param name="page">页码</param>
        /// <returns></returns>
        ProblemList GetProblemList(string problemType, int page);

        /// <summary>
        /// 获取题目内容
        /// </summary>
        /// <param name="type">题目类别</param>
        /// <param name="id">题目Id</param>
        /// <returns></returns>
        ProblemContent GetProblemContent(string type, int id);

        /// <summary>
        /// 获取题目集列表
        /// </summary>
        /// <returns></returns>
        List<ProblemCollection> GetCollections();

        /// <summary>
        /// 获取题目集
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        ProblemCollection GetCollection(string collection);

        /// <summary>
        /// 保存题目集
        /// </summary>
        /// <param name="model"></param>
        /// <param name="GetCollectionName"></param>
        /// <param name="collection"></param>
        /// <returns></returns>
        Result SaveCollection(ProblemCollection model);

        /// <summary>
        /// 获取题目集名称
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        string GetCollectionName(string collection);

        /// <summary>
        /// 删除试题集
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        Result DeleteContest(string collection);

        /// <summary>
        /// 保存题目信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Result SaveProblem(ProblemContent model);

        /// <summary>
        /// 删除题目
        /// </summary>
        /// <param name="collection">试题集</param>
        /// <param name="id">题目编号</param>
        /// <returns></returns>
        Result DeleteProblem(string collection, int? id);

        /// <summary>
        /// 清空试题集
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        Result ClearCollection(string collection);

        /// <summary>
        /// 全部导出
        /// </summary>
        /// <param name="collection"></param>
        IWorkbook Export(string collection);

        /// <summary>
        /// 批量导入
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        Result Import(HttpPostedFileBase file);
    }
}
