﻿using FC.Data.ViewModels.Account;
using FC.Utils;

namespace FC.IService
{
    public interface IAccountService
    {
        /// <summary>
        /// 注册用户
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <returns></returns>
        Result Register(RegisterForm user);

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <returns></returns>
        User Login(LoginForm user);

        /// <summary>
        /// 检查手机号是否已注册
        /// </summary>
        /// <param name="phoneNumber">用户注册手机号</param>
        /// <returns></returns>
        Result CheckPhoneNumber(string phoneNumber);

        /// <summary>
        /// 检查用户名是否已注册
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        Result CheckUserName(string userName);
    }
}
