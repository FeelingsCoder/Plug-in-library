﻿using FC.Data.ViewModels.Weather;
using FC.Weather.PM25;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FC.IService
{
    /// <summary>
    /// 天气服务接口
    /// </summary>
    public interface IWeatherService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="area">地区,(101)全国</param>
        /// <param name="type">天气数据类型 forecast|observe|alarm|air|index</param>
        /// <returns></returns>
        SimpleWeatherModel GetSimpleWeather(string area, string type = "forecast");

        /// <summary>
        /// 获取PM2.5指数及空气质量
        /// </summary>
        /// <param name="city">城市名称</param>
        /// <returns></returns>
        PM2_5 GetPM2_5(string city);
    }
}
